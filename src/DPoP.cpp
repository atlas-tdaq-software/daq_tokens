#include "DPoP.h"
#include "jwt-cpp/traits/nlohmann-json/traits.h"

#include <chrono>
#include <jwt-cpp/base.h>
#include <jwt-cpp/jwt.h>
#include <jwt-cpp/traits/nlohmann-json/defaults.h>

#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <openssl/evp.h>
#include <openssl/pem.h>
#include <memory>
#include <openssl/types.h>
#include <stdexcept>

namespace daq::tokens::details {

    namespace {
        std::string sha256(std::string_view input)
        {
            static std::unique_ptr<EVP_MD, decltype(&EVP_MD_free)> md{EVP_MD_fetch(nullptr, "SHA256", nullptr), EVP_MD_free};

            unsigned char digest[32];
            EVP_Digest(input.data(), input.size(), digest, nullptr, md.get(), nullptr);
            return jwt::base::trim<jwt::alphabet::base64url>(jwt::base::encode<jwt::alphabet::base64url>({ (char *)digest, sizeof(digest) }));
        }
    }

    void DPoPKey::init(EVP_PKEY *key)
    {
        {
            // base64url encode the raw public key
            unsigned char buffer[32];
            size_t size = 32;
            EVP_PKEY_get_raw_public_key(key, buffer, &size);
            m_public_key = jwt::base::trim<jwt::alphabet::base64url>(jwt::base::encode<jwt::alphabet::base64url>({ (char *)buffer, size }));
        }

        {
            // Create a signer for jwt
            char *buf = nullptr;
            size_t length = 0;
            FILE *fp = open_memstream(&buf, &length);
            PEM_write_PrivateKey(fp, key, nullptr, nullptr, 0, nullptr, nullptr);
            fclose(fp);
            std::string privkey(buf, length);
            free(buf);
            m_signer.reset(new jwt::algorithm::ed25519("", privkey));
        }
    }

    DPoPKey::DPoPKey()
    {
        std::unique_ptr<EVP_PKEY, decltype(&EVP_PKEY_free)> key{EVP_PKEY_Q_keygen(nullptr, nullptr, "Ed25519"), EVP_PKEY_free};
        init(key.get());
    }

    DPoPKey::DPoPKey(std::string_view path)
    {
        std::unique_ptr<FILE, decltype(&fclose)> f{fopen(path.data(), "r"), fclose};
        if(f) {
            std::unique_ptr<EVP_PKEY, decltype(&EVP_PKEY_free)> key{PEM_read_PrivateKey(f.get(), nullptr, nullptr, (void *)""), &EVP_PKEY_free};
            if(key) {
                init(key.get());
            } else {
                throw std::runtime_error("DPoPKey: cannot read private key");
            }
        } else {
            throw std::runtime_error("DPoPKey: private key file not found");
        }
    }

    jwt::algorithm::ed25519 DPoPKey::signer() const
    {
        return *m_signer;
    }

    std::string DPoPKey::public_key() const
    {
        return m_public_key;
    }

    std::string DPoPKey::generate_dpop(std::string_view operation, std::string_view token)
    {
        auto uuid{boost::uuids::random_generator()()};
        nlohmann::json jwk = {
            { "kty", "OKP"},
            { "crv", "Ed25519" },
            { "x", m_public_key}
        };

        auto builder = jwt::create()
            .set_type("dpop+jwt")
            .set_header_claim("jwk", jwk)
            .set_payload_claim("htm", "POST")
            .set_payload_claim("htu", operation)
            .set_issued_at(std::chrono::system_clock::now())
            .set_id(boost::uuids::to_string(uuid));

        if(!token.empty()) {
            builder.set_payload_claim("ath", sha256(token));
        }

        return builder.sign(*m_signer);
    }

    std::string jwk_thumbprint(std::string_view b64url_encoded_pubkey)
    {
        // Ed25519: crv, kty, x
        // See RFC7638
        std::string input = R"({"crv":"Ed25519","kty":"OKP","x":")";
        input +=  b64url_encoded_pubkey;
        input +=  R"("})";

        return sha256(input);
    }

    std::string verify_dpop(std::string_view encoded_token, std::string_view operation, std::string_view access_token, unsigned int max_age)
    {
        auto decoded = jwt::decode({ encoded_token.data(), encoded_token.size()});
        jwt::jwk<jwt::traits::nlohmann_json> key{decoded.get_header_claim("jwk").to_json()};
        if(key.get_jwk_claim("kty").as_string() != "OKP" ||
           key.get_jwk_claim("crv").as_string() != "Ed25519") {
            throw std::runtime_error("Unsupported DPoP key type");
        }

        auto raw_key = jwt::base::decode<jwt::alphabet::base64url>(jwt::base::pad<jwt::alphabet::base64url>(key.get_jwk_claim("x").as_string()));
        EVP_PKEY *pkey = EVP_PKEY_new_raw_public_key_ex(nullptr,
                                                        "Ed25519",
                                                        nullptr,
                                                        (unsigned char *)raw_key.data(),
                                                        raw_key.size());
        char *buf;
        size_t length;
        FILE *fp = open_memstream(&buf, &length);
        PEM_write_PUBKEY(fp, pkey);
        fclose(fp);
        std::string pubkey(buf, length);
        free(buf);
        EVP_PKEY_free(pkey);

        jwt::verify()
            .with_type("dpop+jwt")
            .issued_at_leeway(5)
            .allow_algorithm(jwt::algorithm::ed25519(pubkey))
            .verify(decoded);

        if(decoded.get_issued_at() < std::chrono::system_clock::now() - std::chrono::seconds(max_age)) {
            throw std::runtime_error("DPoP token too old");
        }

        if(decoded.get_payload_claim("htm").as_string() != "POST") {
            throw std::runtime_error("Wrong 'htm' claim");
        }

        if(decoded.get_payload_claim("htu").as_string() != operation) {
            throw std::runtime_error("DPoP operation does not match");
        }

        if(!decoded.has_id()) { // TODO: replay check for jti claim
            throw std::runtime_error("Missing 'jti' claim");
        }

        if(!access_token.empty()) {
            if(decoded.get_payload_claim("ath").as_string() != sha256(access_token)) {
                throw std::runtime_error("Invalid 'ath' claim in DPop token");
            }
        }
        return jwk_thumbprint(key.get_jwk_claim("x").as_string());
    }

}
