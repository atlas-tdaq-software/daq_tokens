#include "TokenProvider.h"
#include "sso-helper/sso-helper.h"
#include <nlohmann/json.hpp>
#include <chrono>

namespace daq::tokens::detail {

    class DeviceProvider : public TokenProvider
    {
    public:
        DeviceProvider()
            : TokenProvider("device")
        {}
    protected:
        virtual State acquire_token(daq::tokens::Mode /* mode */, State& /* state */) override
        {
            auto response = nlohmann::json::parse(daq::sso::get_token_from_device_code("atlas-tdaq-token"));
            return State{
                .access_token  = response["access_token"],
                .refresh_token = response["refresh_token"],
                .expires_at = std::chrono::system_clock::now() + std::chrono::seconds(response["expires_in"].get<time_t>()),
                .refresh_expires_at = std::chrono::system_clock::now() + std::chrono::seconds(response["refresh_expires_in"].get<time_t>())};
        }
    };

    namespace {
        DeviceProvider provider;
    }
}
