
#include "DPoP.h"
#include "daq_tokens/verify.h"
#include "ers/ers.h"

#include <chrono>
#include <curl/curl.h>
#include <boost/process.hpp>
#include <openssl/crypto.h>

#include <cstdlib>
#include <mutex>
#include <sstream>
#include <stdexcept>
#include <string_view>
#include <unordered_map>

namespace daq {

    namespace tokens {

        namespace {

            // The keysstore, a mapping from key identifier
            // to actual public key.
            class KeyStore {
            public:

                // Get a key if in store
                std::string get(const std::string& kid)
                {
                    std::scoped_lock lock(m_mutex);
                    auto it = m_store.find(kid);
                    if(it != m_store.end()) {
                        return it->second;
                    }
                    return "";
                }

                // Put a new key into store, calculationg its fingerprint
                void put(const std::string& key, const std::string& kid = "")
                {
                    using namespace boost::process;

                    std::scoped_lock lock(m_mutex);

                    if(!kid.empty()) {
                        m_store[kid] = key;
                    } else {
                        auto md5 = get_fingerprint(key, EVP_md5());
                        m_store[md5] = key;
                        auto sha256 = get_fingerprint(key, EVP_sha256());
                        m_store[sha256] = key;
                    }
                }

            private:

                std::string
                get_fingerprint(const std::string& key, const EVP_MD *md)
                {
                    auto evp_pub = jwt::helper::load_public_key_from_string(key);

                    unsigned char *data = nullptr;
                    int size = i2d_PUBKEY(evp_pub.get(), &data);
                    if(size < 0) { throw std::runtime_error("Cannot convert to DER"); }

                    std::unique_ptr<unsigned char[], decltype(&free)> holder(data, free);

                    // Create digest context, CentOS 8 has newer openssl version.
#if OPENSSL_VERSION_NUMBER < 0x10100000L
                    std::unique_ptr<EVP_MD_CTX, decltype(&EVP_MD_CTX_destroy)> ctx(EVP_MD_CTX_create(), EVP_MD_CTX_destroy);
#else
                    std::unique_ptr<EVP_MD_CTX, decltype(&EVP_MD_CTX_free)> ctx(EVP_MD_CTX_new(), EVP_MD_CTX_free);
#endif

                    if(!ctx) { throw std::runtime_error("Cannot create MD context"); }

                    if(!EVP_DigestInit(ctx.get(), md)) { throw std::runtime_error("Cannot initialize message digest"); }

                    if(!EVP_DigestUpdate(ctx.get(), data, size)) { throw std::runtime_error("Cannot update message digest"); }

                    std::unique_ptr<uint8_t[]> fingerprint(new uint8_t[EVP_MD_CTX_size(ctx.get())]);
                    unsigned int length;

                    if(!EVP_DigestFinal(ctx.get(), fingerprint.get(), &length)) {
                        throw std::runtime_error("Cannot finalize message digest");
                    }

                    // Turn into hex
                    std::stringstream fp;
                    for(decltype(length) i = 0; i < length; i++) {
                        fp << std::hex << std::setw(2) << std::setfill('0') << (unsigned int)fingerprint[i];
                    }
                    return fp.str();
                }

                std::mutex                                   m_mutex;
                std::unordered_map<std::string, std::string> m_store;
            };

            // The only key store
            KeyStore store;

            // Global CURL initialization
            struct CurlInit {
                CurlInit()
                {
                    curl_global_init(CURL_GLOBAL_ALL);
                }
                ~CurlInit()
                {
                    curl_global_cleanup();
                }
            } curl_init;

            // callback function for CURL
            size_t
            receive_data(void *buffer, size_t size, size_t nmemb, void *userp)
            {
                std::string *key = reinterpret_cast<std::string*>(userp);
                key->append(reinterpret_cast<char *>(buffer), nmemb);
                return nmemb * size;
            }

            // Helper function to get public key
            std::string
            get_public_key(const std::string& kid)
            {

                // Check the key store first.
                std::string key = store.get(kid);

                if(!key.empty()) {
                    return key;
                }

                // Key is in environment itself ?
                if(const char *pubkey = getenv("TDAQ_TOKEN_PUBLIC_KEY")) {
                    store.put(pubkey);
                    key = store.get(kid);
                    if(!key.empty()) {
                        return key;
                    }
                }

                // Try to fetch latest public key from URL, CERN is default.
                std::string public_key_url = "https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/certs";

                // location should be in environment
                if(const char *url_from_env = getenv("TDAQ_TOKEN_PUBLIC_KEY_URL")) {
                    public_key_url = url_from_env;
                }

                CURL *handle = curl_easy_init();
                if(!handle) {
                    return "";
                }

                size_t pos = 0;
                size_t delim = public_key_url.find('|');

                while(true) {
                    std::string url = public_key_url.substr(pos, delim);

                    key.clear();

                    CURLcode res;
                    curl_easy_setopt(handle, CURLOPT_URL, url.c_str());
                    curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, receive_data);
                    curl_easy_setopt(handle, CURLOPT_WRITEDATA, &key);
                    if(const char *connect_timeout = getenv("TDAQ_TOKEN_PUBLIC_KEY_TIMEOUT")) {
                        long timeout_in_seconds = std::stoul(connect_timeout, nullptr);
                        curl_easy_setopt(handle, CURLOPT_CONNECTTIMEOUT, timeout_in_seconds);
                    }
                    res = curl_easy_perform(handle);

                    try {
                        if(res != CURLE_OK) {
                            throw std::runtime_error(curl_easy_strerror(res));
                        }

                        if(key.find("-----BEGIN PUBLIC KEY-----") == 0) {
                            while(key.find("-----BEGIN PUBLIC KEY-----") == 0) {
                                store.put(key);
                                key = key.substr(key.find("-----END PUBLIC KEY-----") + 25);
                            };
                        } else {
                            auto jwks = nlohmann::json::parse(key);
                            for(auto& jwk : jwks["keys"]) {

                                if(jwk["kty"] != "RSA" ||
                                   jwk["alg"] != "RS256" ||
                                   jwk["use"] != "sig") {
                                    continue;
                                }

                                key = jwt::helper::convert_base64_der_to_pem(jwk["x5c"][0]);
                                store.put(key, jwk["kid"]);
                            }
                        }
                    } catch (std::exception& ex) {
                      // This catches both transport errors and
                      // exceptions caused by invalid payload.
                      // The best we can do is to go to the next
                      // entry.
                        ERS_DEBUG(1, "Retrieving or decoding public key failed: " << ex.what());
                    }

                    if(delim == std::string::npos) {
                        break;
                    }

                    pos += delim + 1;
                    delim = public_key_url.find('|', pos);
                    if (delim != std::string::npos) {
                        delim -= pos;
                    }
                }

                curl_easy_cleanup(handle);

                return store.get(kid);
            }

            // replay protection cache
            std::mutex replay_mutex;
            std::list<std::pair<std::chrono::system_clock::time_point, std::string>> replay_cache;

            void add_replay_cache(std::string_view token, unsigned int max_age)
            {
                std::scoped_lock lock(replay_mutex);
                replay_cache.push_back(std::pair(std::chrono::system_clock::now() + std::chrono::seconds(max_age), std::string(token)));
            }

            void check_replay_cache(std::string_view token)
            {
                std::scoped_lock lock(replay_mutex);
                auto now = std::chrono::system_clock::now();
                while(!replay_cache.empty() && replay_cache.front().first < now) {
                    replay_cache.pop_front();
                }
                if(std::find_if(replay_cache.begin(),
                                replay_cache.end(),
                                [token](auto& item) { return item.second == token; }) != replay_cache.end()) {
                    throw std::runtime_error("DPoP proof is being replayed");
                }
            }

        } // anonymous namespace

        Token
        verify(std::string_view encoded_token, std::string_view operation, unsigned int max_age)
        {
            try {

                std::string_view access_token = encoded_token;
                std::string_view dpop_token;

                size_t n = encoded_token.find(':');
                if(n != std::string_view::npos) {
                    access_token = encoded_token.substr(0, n);
                    dpop_token   = encoded_token.substr(n+1);
                }

                if(!dpop_token.empty()) {
                    check_replay_cache(dpop_token);
                }

                Token token = jwt::decode(std::string(access_token));

                auto public_key = get_public_key(token.get_key_id());

                std::string issuer = "https://auth.cern.ch/auth/realms/cern";
                if(const char *env_issuer = getenv("TDAQ_TOKEN_ISSUER")) {
                    issuer = env_issuer;
                }

                auto verifier = jwt::verify().
                    allow_algorithm(jwt::algorithm::rs256{public_key}).
                    with_issuer(issuer).
                    with_audience("atlas-tdaq-token").
                    leeway(5);
                ;

                verifier.verify(token);

                // if either of the following is true:
                //   * dpop_token must exist
                //   * access_token must have a "cnf" claim
                //   * dpop_token must verify under 'operation' and 'access_token'
                //   *   dpop["ath"] must equal sha256(access_token)
                //   * access_token["cnf"]["jkt"] must equal thumbprint(dpop_token.header[jwk])

                if(token.has_payload_claim("cnf") || !dpop_token.empty()) {
                    if(!token.has_payload_claim("cnf")) throw std::runtime_error("Token has no 'cnf' member");
                    if(dpop_token.empty()) throw std::runtime_error("No DPoP token");
                    std::string jkt = details::verify_dpop(dpop_token, operation, access_token, max_age);
                    if(token.get_payload_json()["cnf"]["jkt"] != jkt) throw std::runtime_error("Token is not bound to DPoP key");
                }

                if(!dpop_token.empty()) {
                    add_replay_cache(dpop_token, max_age);
                }

                return token;
            } catch (std::exception& ex) {
                throw CannotVerifyToken(ex);
            } catch(...) {
                throw;
            }
        }
    }
}
