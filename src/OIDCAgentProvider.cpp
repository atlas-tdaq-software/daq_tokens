#include "TokenProvider.h"
#include <nlohmann/json.hpp>
#include <chrono>
#include <boost/asio.hpp>

namespace daq::tokens::detail {

    class OIDCAgentProvider : public TokenProvider
    {
    public:
        OIDCAgentProvider()
            : TokenProvider("agent")
        {}
    protected:

        virtual State acquire_token(daq::tokens::Mode /* mode */, State& /* state */) override
        {
            auto response = get_sso_token_from_agent();
            return State{
                .access_token = response["access_token"],
                .expires_at = std::chrono::system_clock::from_time_t(response["expires_at"].get<time_t>())
            };
        }

        nlohmann::json
        get_sso_token_from_agent()
        {
            std::string buffer;
            nlohmann::json reply;
            if(const char *oidc_path = getenv("OIDC_SOCK")) {
                boost::asio::local::stream_protocol::iostream sock;
                sock.connect(boost::asio::local::stream_protocol::endpoint(oidc_path));
                nlohmann::json request = {
                    { "request", "access_token" },
                    { "account", "atlas-tdaq-token" },
                    // { "issuer", "https://auth.cern.ch/auth/realms/cern" },
                    { "min_valid_period",  60 },
                    { "application_hint", "TDAQ Application" },
                    // { "scope", "openid profile email" },
                    // { "audience", "atlas-tdaq-token" }
                };
                sock << request.dump();
                sock >> buffer;
                sock.close();
                // If we got this far and receive nothing, it's an error.
                // Don't try to hide it by continuing.
                if(buffer.empty()) {
                    throw std::runtime_error("Received no token from OIDC socket");
                }

                reply = nlohmann::json::parse(buffer);
            }
            return reply;
        }

    };

    namespace {
        OIDCAgentProvider provider;
    }
}


