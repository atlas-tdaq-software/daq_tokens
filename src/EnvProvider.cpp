#include "TokenProvider.h"
#include <jwt-cpp/jwt.h>
#include <jwt-cpp/traits/nlohmann-json/defaults.h>
#include <chrono>
#include <fstream>

namespace daq::tokens::detail {

    class EnvProvider : public TokenProvider
    {
    public:
        EnvProvider()
            : TokenProvider("env")
        {}
    protected:
        virtual State acquire_token(daq::tokens::Mode /* mode */, State& /* state */) override
        {
            if(const char *char_token = getenv("BEARER_TOKEN")) {
                std::string token(char_token);
                auto decoded = jwt::decode(token.substr(0, token.rfind(':')));
                if(decoded.get_expires_at() > std::chrono::system_clock::now() - std::chrono::seconds(60)) {
                    return State{
                        .access_token = token,
                        .expires_at = decoded.get_expires_at()};
                }
            }

            std::string file_name;
            if(const char *filename = getenv("BEARER_TOKEN_FILE")) {
                file_name = filename;
            } else {
                uid_t uid = geteuid();
                if(const char *runtime = getenv("XDG_RUNTIME_DIR")) {
                    file_name = runtime;
                    file_name += "/bt_u";
                } else {
                    file_name = "/tmp/bt_u";
                }
                file_name += std::to_string(uid);
                file_name += "-atlas-tdaq";
            }

            std::ifstream stream(file_name);
            if(stream) {
                std::string token;
                getline(stream, token);
                auto decoded = jwt::decode(token.substr(0, token.rfind(':')));
                if(decoded.get_expires_at() > std::chrono::system_clock::now() - std::chrono::seconds(60)) {
                    return State{
                        .access_token = token,
                        .expires_at = decoded.get_expires_at()
                    };
                }
            }
            return {};
        }
    };

    namespace {
        EnvProvider provider;
    }
}
