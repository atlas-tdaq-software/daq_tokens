
#include "daq_tokens/acquire.h"
#include "daq_tokens/verify.h"
#include "daq_tokens/credentials.h"
#include <cstdlib>
#include <pwd.h>
#include <string>

namespace daq::tokens {

    std::string
    get_credentials(Mode mode, std::string_view operation)
    {
        static std::string login;
        static uid_t       uid = 0;
        if(enabled()) {
            return acquire(mode, { operation.data(), operation.size()} );
        } else {
            uid_t current_uid = geteuid();
            if(uid == 0 || current_uid != uid) {
                char buf[16000];
                passwd pwd;
                passwd *result = nullptr;
                if (getpwuid_r(current_uid, &pwd, buf, sizeof(buf), &result) == 0 && result != nullptr) {
                    login = pwd.pw_name;
                } else {
                    login = "-unknown-";
                }
                uid = current_uid;
            }
            return login;
        }
    }

    std::string
    verify_credentials(const std::string& creds, std::string_view operation, unsigned int max_age)
    {
        if(enabled()) {
            return verify(creds, operation, max_age).get_subject();
        } else {
            return creds;
        }
    }
}
