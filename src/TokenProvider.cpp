#include "TokenProvider.h"

namespace daq::tokens::detail {

    TokenProvider::TokenProvider(const std::string& name)
    {
        registry()[name] = this;
    }

    State TokenProvider::acquire(const std::string& method, daq::tokens::Mode mode, State& state, std::string_view operation)
    {
        auto& providers = registry();
        auto entry = providers.find(method);
        if(entry != providers.end()) {
            return entry->second->acquire_token(mode, state);
        }
        return {};
    }

    void TokenProvider::new_token(const State& state)
    {
        for(auto& entry : registry()) {
            entry.second->new_token_added(state);
        }
    }

    std::map<std::string,TokenProvider*>&
    TokenProvider::registry()
    {
        static std::map<std::string,TokenProvider*> providers;
        return providers;
    }

}
