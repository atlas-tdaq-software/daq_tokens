
#include "daq_tokens/acquire.h"
#include "TokenProvider.h"
#include "sso-helper/sso-helper.h"
#include <boost/algorithm/string.hpp>
#include <nlohmann/json.hpp>
#include <mutex>
#include <exception>
#include <chrono>
#include <stdexcept>
#include <string>

namespace daq {
    namespace tokens {

        namespace {

            // safety leeway for checks of token expiry = 60 seconds
            const std::chrono::seconds leeway{60};

            // for multiple thread synchronization
            std::mutex  mutex;

            // State of current token
            detail::State  current;
        }

        std::string
        acquire(Mode mode, const std::string& operation)
        {
            // only one thread at a time
            std::scoped_lock lock(mutex);

            // The last exception caught.
            // If it is not null when we run out of methods
            // we re-throw it instead to get a hint of what went wrong;
            // otherwise all error information is gone.
            std::exception_ptr eptr = nullptr;

            try {

                // Check if we can re-use the last token
                if(mode == Mode::Reuse &&
                   !current.access_token.empty() &&
                   std::chrono::system_clock::now() < current.expires_at - leeway) {
                    // Existing token still good
                    if(current.signer) {
                        return current.access_token + ":" + current.signer(operation, current.access_token);
                    } else {
                        return current.access_token;
                    }
                }

                if(std::chrono::system_clock::now() > current.expires_at &&
                   !current.refresh_token.empty()) {
                    // token expired but we have a refresh token, try it
                    try {
                        auto response = nlohmann::json::parse(daq::sso::get_token_from_refresh_token("atlas-tdaq-token", current.refresh_token));
                        // we simply try to access the fields, if we got an error response this throws an exception
                        detail::State new_state{.access_token       = response["access_token"],
                                                .refresh_token      = response["refresh_token"],
                                                .expires_at         = std::chrono::system_clock::now() + std::chrono::seconds(response["expires_in"].get<time_t>()),
                                                .refresh_expires_at = std::chrono::system_clock::now() + std::chrono::seconds(response["refresh_expires_in"].get<time_t>())};
                        // refresh token worked, maybe update state and return new access token
                        if(mode == Mode::Reuse) {
                            current = new_state;
                        }
                        return new_state.access_token;
                    } catch (...) {
                        eptr = std::current_exception();
                    }
                }

                // new token required and no refresh token
                current = detail::State{};

                // default methods if nothing configured
                std::vector<std::string> methods{"local", "http"};

                if(const char *method_var = getenv("TDAQ_TOKEN_ACQUIRE")) {
                    methods.clear();
                    boost::algorithm::split(methods, method_var,  boost::algorithm::is_space());
                }

                // Try each configured method in turn
                for(auto& method : methods) {
                    try {
                        auto response = detail::TokenProvider::acquire(method, mode, current, operation);
                        if(!response.access_token.empty()) {
                            if(mode == Mode::Reuse) {
                                current = response;
                                detail::TokenProvider::new_token(response);
                            }
                            if(response.signer) {
                                auto dopd_token = response.signer(operation, response.access_token);
                                return response.access_token + ':' + dopd_token;
                            }
                            return response.access_token;
                        }
                    } catch(...) {
                        eptr = std::current_exception();
                    }
                }

                // out of methods to try
                if(eptr) {
                    std::rethrow_exception(eptr);
                } else {
                    throw std::runtime_error("No more methods to acquire token");
                }

            } catch (std::exception& ex) {
                throw CannotAcquireToken(ex);
            } catch(...) {
                // we don't expect any non-std derived exceptions
                throw;
            }
        }
    }
}
