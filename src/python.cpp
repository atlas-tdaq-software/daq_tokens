
#include <nanobind/nanobind.h>
#include <nanobind/stl/string.h>
#include <nanobind/stl/string_view.h>
#include <string_view>

#include "daq_tokens/acquire.h"
#include "daq_tokens/verify.h"

namespace nb = nanobind;

namespace {

    std::string verify(std::string_view encoded_token, std::string_view operation = {}, unsigned int max_age = 15)
    {
        return daq::tokens::verify(encoded_token, operation, max_age).get_payload();
    }

}

NB_MODULE(pydaq_tokens, m)
{
    nb::enum_<daq::tokens::Mode>(m, "Mode")
        .value("Reuse", daq::tokens::Mode::Reuse)
        .value("Fresh", daq::tokens::Mode::Fresh)
        ;

    m.def("acquire",
          (std::string (*)(daq::tokens::Mode, const std::string&))&daq::tokens::acquire,
          nb::arg("mode") = daq::tokens::Mode::Reuse,
          nb::arg("operation") = "");
    m.def("acquire", (std::string (*)(const std::string&))&daq::tokens::acquire);
    m.def("verify", &verify, nb::arg("encoded"), nb::arg("operation") = "" , nb::arg("max_age") = 15);
}
