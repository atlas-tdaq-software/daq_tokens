#include "TokenProvider.h"
#include "DPoP.h"
#include <curl/curl.h>
#include <chrono>
#include <functional>

#include <jwt-cpp/jwt.h>
#include <jwt-cpp/traits/nlohmann-json/defaults.h>

namespace {
    // callback function for CURL
    size_t
    receive_data(void *buffer, size_t size, size_t nmemb, void *userp)
    {
        std::string *key = reinterpret_cast<std::string*>(userp);
        key->append(reinterpret_cast<char *>(buffer), nmemb);
        return nmemb * size;
    }
}

namespace daq::tokens::detail {

    class HTTPProvider : public TokenProvider
    {
    public:
        HTTPProvider()
            : TokenProvider("http")
        {}
    protected:
        virtual State acquire_token(daq::tokens::Mode /* mode */, State & /* state */) override
        {
            std::unique_ptr<CURL, decltype(&curl_easy_cleanup)> curl{curl_easy_init(), curl_easy_cleanup};
            auto handle = curl.get();
            // curl_easy_setopt(handle, CURLOPT_VERBOSE, 1);
            std::string url = "https://vm-atdaq-token.cern.ch/token";
            if(auto url_env = getenv("TDAQ_TOKEN_HTTP_URL")) {
                url = url_env;
            }
            auto timeout = getenv("TDAQ_TOKEN_HTTP_TIMEOUT");
            curl_easy_setopt(handle, CURLOPT_URL, url.c_str());
            curl_easy_setopt(handle, CURLOPT_POSTFIELDS, "");
            curl_easy_setopt(handle, CURLOPT_HTTPAUTH, CURLAUTH_NEGOTIATE);
            curl_easy_setopt(handle, CURLOPT_USERPWD, ":");
            curl_easy_setopt(handle, CURLOPT_CONNECTTIMEOUT, timeout ? strtoul(timeout, nullptr, 10) : 2);
            curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 1);

            auto dpop = m_key.generate_dpop(url.c_str());
            curl_slist* list = nullptr;
            list = curl_slist_append(list, ("DPoP: " + dpop).c_str());
            curl_easy_setopt(handle, CURLOPT_HTTPHEADER, list);

            std::string response;
            curl_easy_setopt(handle, CURLOPT_WRITEDATA, &response);
            curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, receive_data);
            CURLcode ret = curl_easy_perform(handle);
            curl_slist_free_all(list);
            if(ret != CURLE_OK) {
                std::string msg{"curl error: "};
                throw std::runtime_error(msg + curl_easy_strerror(ret));
            }

            long response_code;
            curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &response_code);
            if(response_code != 200) {
                std::string msg("Server response code: ");
                throw std::runtime_error(msg + std::to_string(response_code));
            }

            if(!response.empty()) {
                if(jwt::decode(response).has_payload_claim("cnf")) {
                    return State{
                        .access_token = response,
                        .expires_at = std::chrono::system_clock::now() + lifetime,
                        .signer = std::bind(&details::DPoPKey::generate_dpop, &m_key, std::placeholders::_1, std::placeholders::_2)
                    };
                } else {
                    return State{
                        .access_token = response,
                        .expires_at = std::chrono::system_clock::now() + lifetime
                    };
                }
            }
            return {};
        }
    private:
        details::DPoPKey m_key;
    };

    namespace {
        HTTPProvider provider;
    }
}
