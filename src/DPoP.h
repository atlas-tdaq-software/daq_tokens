#ifndef DAQ_TOKENS_DPOP_H_
#define DAQ_TOKENS_DPOP_H_

#include <string>
#include <string_view>
#include <jwt-cpp/jwt.h>

namespace daq {
    namespace tokens {
        namespace details {

            /**
             * Helper class to maintain Ed25519 for DPoP tokens
             */
            class DPoPKey
            {
            public:
                /// Create a random key
                DPoPKey();
                /// Load key from PEM file
                explicit DPoPKey(std::string_view path);

                /// Return jwt signer object to use as algorithm
                jwt::algorithm::ed25519 signer() const;

                /// Return base64url encoded public key
                std::string public_key() const;

                /// Generate a DPoP token for the given URI operation and the optional access token
                std::string generate_dpop(std::string_view operation, std::string_view token = {});

            private:
                void init(EVP_PKEY *key);
                // base64url encoded public key
                std::string                              m_public_key;
                std::unique_ptr<jwt::algorithm::ed25519> m_signer;
            };

            /**
             * Calculate JWK thumbprint according to RFC7638 for Ed25519 public key.
             *
             * \param     public_key A base64url encoded Ed25519 raw key (kty = OKP and crv = Ed25519 are implied)
             * \returns              The JWK thumbprint encoded with base64url
             */
            std::string jwk_thumbprint(std::string_view b64url_encoded_pubkey);

            /**
             * Generate a DPoP token
             *
             * \param   public_key   A base64url encoded raw public Ed25519 key (the "x" component in the jwk)
             * \param   operation    An URI denoting the requested operation
             * \param   signer       A JWT signer algorithm using the private part of 'key'
             * \param   access_token An optional access token to which the DPoP token is bound
             * \returns              A base64url encoded DPoP token, optionally bound to the access token
             */
            std::string generate_dpop(std::string_view public_key, std::string_view operation, jwt::algorithm::ed25519 signer, std::string_view token = {});

            /**
             * Verify DPoP encoded token
             *
             * \param   encoded_token A base64url encoded DPoP+jwt token (RFC 9449), using an Ed25519 key
             * \param   operation     A URL string describing the operation ("htu" field)
             * \param   access_token  A base64url encoded associated access token (optional)
             * \param   max_age       Maximum allowed age of DPoP proof in seconds
             * \returns               The thumbprint of the public key contained in the token
             * \throws                std::runtime_error
             */
            std::string verify_dpop(std::string_view encoded_token, std::string_view operation, std::string_view access_token = {}, unsigned int max_age = 15);
        }
    }
}

#endif // DAQ_TOKENS_DPOP_H_
