#include "TokenProvider.h"
#include "sso-helper/sso-helper.h"
#include <nlohmann/json.hpp>
#include <chrono>
#include <fstream>
#include <sys/stat.h>

namespace daq::tokens::detail {

    class CacheProvider : public TokenProvider
    {
    public:
        CacheProvider()
            : TokenProvider("cache")
        {}
    protected:

        virtual State acquire_token(daq::tokens::Mode /* mode */, State& /* state */) override
        {
            auto response = get_sso_token_from_cache();
            return State{
                .access_token = response["access_token"],
                .refresh_token = response["refresh_token"],
                .expires_at = std::chrono::system_clock::now() + std::chrono::seconds(response["expires_in"].get<time_t>()),
                .refresh_expires_at = std::chrono::system_clock::now() + std::chrono::seconds(response["refresh_expires_in"].get<time_t>())};
        }

        void new_token_added(const State& state) override
        {
            if(!state.refresh_token.empty()) {
                write_sso_cache(state.refresh_token);
            }
        }

    private:

        // Construct the file name for the cache
        std::string
        get_sso_refresh_token_file_name()
        {
            static std::string file_name = getenv("TDAQ_TOKEN_CACHE") ? getenv("TDAQ_TOKEN_CACHE") : "";
            if(file_name.empty()) {
                if(const char *home = getenv("HOME")) {
                    file_name = home;
                    file_name += "/.cache";
                    std::filesystem::create_directories(file_name);
                    file_name += "/tdaq-sso-cache";
                } else {
                    file_name = "/tmp/tdaq-sso-cache-";
                    file_name += getlogin();
                }
            }
            return file_name;
        }

        // Read the cached refresh token if it exists and try to get a new token.
        nlohmann::json
        get_sso_token_from_cache()
        {
            auto name = get_sso_refresh_token_file_name();
            try {
                // Make sure nobody else can read it, else abort mission
                auto perms = std::filesystem::status(name).permissions();
                if((perms & ~(std::filesystem::perms::owner_all)) != std::filesystem::perms::none) {
                    return "";
                }
            } catch(...) {
                return "";
            }

            std::ifstream f(name);
            std::string result;
            if(f) {
                getline(f, result);
            }

            // We may have a valid refresh token, let's try to get a new one
            return nlohmann::json::parse(daq::sso::get_token_from_refresh_token("atlas-tdaq-token", result));
        }

        // Save the refresh token to the cache
        void write_sso_cache(const std::string& data)
        {
            // We write the output to a temporary file, then rename it to the
            // real destination.
            auto final_name = get_sso_refresh_token_file_name();
            auto name = final_name + ".XXXXXX";
            int fd = mkstemp(&name[0]);
            if(fd == -1) return;
            fchmod(fd, S_IRUSR | S_IWUSR);
            close(fd);

            std::ofstream f(name);
            if(f) {
                f << data;
                f.close();
                // rename() is atomic
                std::filesystem::rename(name, final_name);
            }
        }
    };

    namespace {
        CacheProvider provider;
    }
}
