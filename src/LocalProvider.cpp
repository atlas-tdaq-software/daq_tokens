#include "TokenProvider.h"
#include "DPoP.h"
#include <boost/algorithm/string.hpp>
#include <boost/asio.hpp>
#include <chrono>
#include <cstdio>
#include <exception>

#include <jwt-cpp/jwt.h>
#include <jwt-cpp/traits/nlohmann-json/traits.h>
#include <jwt-cpp/traits/nlohmann-json/defaults.h>

namespace daq::tokens::detail {

    class LocalProvider : public TokenProvider
    {
    public:
        LocalProvider()
            : TokenProvider("local")
        {
        }
    protected:
        virtual State acquire_token(daq::tokens::Mode /* mode */, State& /* state */) override
        {
            std::vector<std::string> paths{"/run/tdaq_token"};
            // Check if we should use local Unix socket
            if(const char *path_var = getenv("TDAQ_TOKEN_PATH")) {

                boost::algorithm::split(paths, path_var, [](char c) { return c == ':'; });
            }
            std::string buffer;
            std::exception_ptr eptr;
            for(auto& path : paths) {
                try {
                    boost::asio::local::stream_protocol::iostream sock;
                    sock.connect(boost::asio::local::stream_protocol::endpoint(path));

                    std::string dpop = m_key.generate_dpop(path);
                    sock << dpop << std::endl;

                    sock.socket().shutdown(boost::asio::local::stream_protocol::socket::shutdown_send);
                    sock >> buffer;
                    sock.close();
                    // If we got this far and receive nothing, it's an error.
                    // Don't try to hide it by continuing.
                    if(buffer.empty()) {
                        throw std::runtime_error("Received no token from socket");
                    }
                    break;
                } catch (std::exception& ex) {
                    eptr = std::current_exception();
                    continue;
                }
            }

            if(buffer.empty()) {
                std::rethrow_exception(eptr);
            }

            if(jwt::decode(buffer).has_payload_claim("cnf")) {
                return State{
                    .access_token = buffer,
                    .expires_at = std::chrono::system_clock::now() + lifetime,
                    .signer = std::bind(&details::DPoPKey::generate_dpop, &m_key, std::placeholders::_1, std::placeholders::_2)
                };
            } else {
                return State{
                    .access_token = buffer,
                    .expires_at = std::chrono::system_clock::now() + lifetime,
                };
            }
        }
    private:
        details::DPoPKey m_key;
    };

    namespace {
        LocalProvider provider;
    }
}
