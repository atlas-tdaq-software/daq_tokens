#ifndef DAQ_TOKENS_TOKEN_PROVIDER_H_
#define DAQ_TOKENS_TOKEN_PROVIDER_H_

#include <chrono>
#include <string>
#include <map>
#include <functional>
#include "daq_tokens/acquire.h"
#include <jwt-cpp/jwt.h>

namespace daq {
    namespace tokens {

        const std::chrono::seconds lifetime{1200};

        namespace detail {


            struct State
            {
                std::string access_token;
                std::string refresh_token;
                std::chrono::system_clock::time_point expires_at;
                std::chrono::system_clock::time_point refresh_expires_at;
                std::function<std::string (std::string_view, std::string_view)> signer;
            };

            /**
             * Abstract base class for token providers.
             */
            class TokenProvider
            {
            protected:
                explicit TokenProvider(const std::string& name);
            public:
                virtual  ~TokenProvider() {}

                // User interface, find provider and call its acquire_token() method
                static State acquire(const std::string& method, daq::tokens::Mode mode, State& state, std::string_view operation);

                // Callback to inform provider about a new token
                static void new_token(const State& state);

            protected:
                virtual State acquire_token(daq::tokens::Mode mode, State& state) = 0;
                virtual void  new_token_added(const State& /* state */) {}
                static std::map<std::string, TokenProvider*>& registry();
            };
        }
    }
}

#endif // DAQ_TOKENS_TOKEN_PROVIDER_H_
