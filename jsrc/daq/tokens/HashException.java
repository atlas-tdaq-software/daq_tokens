package daq.tokens;

@SuppressWarnings("serial")
public class HashException extends ers.Issue {

    /**
     * The hash function encountered an error
     *
     * @param reason Clear text reason
     * @param cause The cause of this exception
     *
     */
    public HashException(final String reason, final Throwable cause) {
        super(reason, cause);
    }

    /**
     * The hash function encountered an error
     *
     * @param reason Clear text reason
     */
    public HashException(final String reason) {
        super(reason);
    }
}
