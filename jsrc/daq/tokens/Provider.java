package daq.tokens;

import daq.tokens.internal.Mode;

public interface Provider {
    String getToken(Mode mode);
}
