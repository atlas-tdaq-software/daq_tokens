package daq.tokens;

import java.util.Map;

import daq.tokens.internal.Mode;
import daq.tokens.details.JWTAcquirer;
import daq.tokens.details.JWTCommon;
import daq.tokens.details.JWTVerifier;
import daq.tokens.Provider;

/**
 * Utility class to acquire and verify a JSON web token.
 */
public class JWToken {
    /**
     * Enumeration used when a token is acquired.
     */
    public enum MODE {
        /**
         * A new token will be requested
         */
        FRESH(Mode.Fresh),

        /**
         * A previous valid token will be re-used
         */
        REUSE(Mode.Reuse);

        private final Mode orig;

        MODE(final Mode mode) {
            this.orig = mode;
        }

        Mode getOriginal() {
            return this.orig;
        }
    }

    private JWToken() {
    }

    /// The default maximum age of the DPoP proof
    protected static final int MAX_AGE = 15;

    /**
     * It acquires a JSON web token for a given operation
     *
     * @param mode Whether a new token should be created or a previous valid token should be re-used
     * @param operation A URI describing the desired operation
     * @return The JSON web token as a string
     * @throws AcquireTokenException The token could not be acquired
     */
    public static String acquire(final MODE mode, final String operation) throws AcquireTokenException {
        // The following is a hack around the mixed language environment
        // If getenv("TDAQ_TOKEN_ACQUIRE") contains "callback" we use
        // our internal provider instead of calling into C++.
        // If a null pointer is returned we execute the normal code path.
        if(System.getenv("TDAQ_TOKEN_ACQUIRE").contains("callback") &&
           provider != null) {
            String token = provider.getToken(mode.getOriginal());
            if(token != null) {
                return token;
            }
        }
        return JWTAcquirer.acquire(mode.getOriginal(), operation);
    }

    /**
     * It acquires a JSON web token
     *
     * @param mode Whether a new token should be created or a previous valid token should be re-used
     * @return The JSON web token as a string
     * @throws AcquireTokenException The token could not be acquired
     */
    public static String acquire(final MODE mode) throws AcquireTokenException {
        return acquire(mode, "");
    }

    /**
     * It acquires a JSON web token
     *
     * @param operation A URI describing the desired operation
     * @return The JSON web token as a string
     * @throws AcquireTokenException The token could not be acquired
     */
    public static String acquire(final String operation) throws AcquireTokenException {
        return acquire(MODE.REUSE, operation);
    }

    /**
     * It acquires a new fresh JSON web token
     *
     * @return The JSON web token as a string
     * @throws AcquireTokenException The token could not be acquired
     */
    public static String acquire() throws AcquireTokenException {
        return acquire(MODE.REUSE, "");
    }

    /**
     * It verifies a JSON web token
     *
     * @param token The token to be verified
     * @return A map holding the decoded properties of the token
     * @throws VerifyTokenException The token could not be verified
     */
    public static Map<String, Object> verify(final String token) throws VerifyTokenException {
        return JWTVerifier.verify(token, "", MAX_AGE);
    }

    /**
     * It verifies a JSON web token for a given operation
     *
     * @param token The token to be verified
     * @param operation An URI representing the operation
     * @return A map holding the decoded properties of the token
     * @throws VerifyTokenException The token could not be verified
     */
    public static Map<String, Object> verify(final String token, final String operation) throws VerifyTokenException {
        return JWTVerifier.verify(token, operation, MAX_AGE);
    }

    /**
     * It verifies a JSON web token for a given operation
     *
     * @param token The token to be verified
     * @param operation An URI representing the operation
     * @param max_age Maximum allowed age of DPoP proof
     * @return A map holding the decoded properties of the token
     * @throws VerifyTokenException The token could not be verified
     */
    public static Map<String, Object> verify(final String token, final String operation, int max_age) throws VerifyTokenException {
        return JWTVerifier.verify(token, operation, max_age);
    }

    /**
     * It checks whether the JSON token mechanism is enabled for the current process
     *
     * @return <em>true</em> if the JSON token mechanism is enabled for the current process
     */
    public static boolean enabled() {
        return JWTCommon.enabled();
    }

    /**
     * Set a new provider callback if method == 'callback'
     */
    public static Provider setProvider(Provider prov) {
        Provider old = provider;
        provider = prov;
        return old;
    }

    private static Provider provider = null;
}
