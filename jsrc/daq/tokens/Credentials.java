package daq.tokens;

/**
 * Helper class to get and verify user credentials.
 *
 * If authentication checks are not enabled the get() functions
 * will just return the user name, otherwise a DAQ token.
 */
public class Credentials {

    private static final boolean CHECKS_ENABLED = JWToken.enabled();
    private static final String USER_NAME = System.getProperty("user.name");

    public static String get() throws AcquireTokenException {
        return get(JWToken.MODE.REUSE, "");
    }

    public static String get(String operation) throws AcquireTokenException {
        return get(JWToken.MODE.REUSE, operation);
    }

    public static String get(JWToken.MODE mode) throws AcquireTokenException {
        return get(mode, "");
    }

    public static String get(JWToken.MODE mode, String operation) throws AcquireTokenException {
        if(CHECKS_ENABLED) {
            return JWToken.acquire(mode, operation);
        } else {
            return USER_NAME;
        }
    }

    public static String verify(String credentials) throws VerifyTokenException {
        return verify(credentials, "", JWToken.MAX_AGE);
    }

    public static String verify(String credentials, String operation) throws VerifyTokenException {
        return verify(credentials, operation, JWToken.MAX_AGE);
    }

    public static String verify(String credentials, int max_age) throws VerifyTokenException {
        return verify(credentials, "", max_age);
    }

    public static String verify(String credentials, String operation, int max_age)  throws VerifyTokenException {
        if(CHECKS_ENABLED) {
            return (String )JWToken.verify(credentials, operation, max_age).get("sub");
        } else {
            return credentials;
        }
    }

};
