package daq.tokens.details;

import java.util.Map;
import java.util.HashMap;

import daq.tokens.VerifyTokenException;
import daq.tokens.internal.JDaqTokens;
import daq.tokens.internal.Mode;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.Gson;

public class JWTVerifier {
    static {
        try {
            System.loadLibrary("jdaq_tokens");
        }
        catch(final UnsatisfiedLinkError ex) {
            ex.printStackTrace();
            System.exit(-1);
        }
    }

    private static class VerifyTokenError extends RuntimeException {
        private static final long serialVersionUID = -3987478280231296699L;

        VerifyTokenError(final String message, final Throwable cause) {
            super(message, cause);
        }
    }

    private JWTVerifier() {
    }

    public static synchronized Map<String, Object> verify(final String token, final String operation, int max_age) throws VerifyTokenException {
        String payload = JDaqTokens.verify(token, operation, max_age);
        return new Gson().fromJson(payload, HashMap.class);
    }
}
