package daq.tokens;

import java.util.Arrays;
import java.util.Collection;
import java.security.MessageDigest;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import io.jsonwebtoken.io.Encoders;
import java.nio.charset.StandardCharsets;

public class Hash {

    private static final void hash_object(MessageDigest digest, Object obj) {
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        if(obj instanceof Collection<?>) {
            Collection<?> coll = (Collection<?>)obj;
            buffer.putLong(coll.size());
            digest.update(buffer.array());
            for(Object o : coll) {
                hash_object(digest, o);
            }
        } else {
            byte[] s = obj.toString().getBytes(StandardCharsets.UTF_8);
            buffer.putLong(s.length);
            digest.update(buffer.array());
            digest.update(s);
        }
    }

    public static final String hash(Object... args) throws HashException {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");

            for(Object obj : args) {
                hash_object(digest, obj);
            }
            byte[] result = Arrays.copyOfRange(digest.digest(), 0, 16);
            return Encoders.BASE64URL.encode(result);
        } catch(final java.security.NoSuchAlgorithmException ex) {
            throw new HashException(ex.getMessage(), ex);
        }
    }
}
