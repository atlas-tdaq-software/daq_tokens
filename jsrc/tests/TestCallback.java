package tests;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;

import java.util.Map;

import daq.tokens.AcquireTokenException;
import daq.tokens.JWToken;
import daq.tokens.JWToken.MODE;
import daq.tokens.VerifyTokenException;
import daq.tokens.Provider;

class MyCallback implements Provider {

    static final String stale = "boogaboo!";
    static final String fresh = "fresh boogaboo!";
    public String getToken(daq.tokens.internal.Mode mode)
    {
        if(mode == daq.tokens.internal.Mode.Fresh) {
            return fresh;
        } else {
            return stale;
        }
    }
}

public class TestCallback {

    public TestCallback() {}

    @Before
    public void setUp()
    {
        JWToken.setProvider(new MyCallback());
    }

    @Test
    public void checkAcquireCallbackFresh()
    {
        try {
            String token = JWToken.acquire(MODE.FRESH);
            assertNotNull(token);
            assertEquals(token, MyCallback.fresh);
        } catch (AcquireTokenException ex) {
            fail();
        }
    }

    @Test
    public void checkAcquireCallbackReuse()
    {
        try {
            String token = JWToken.acquire(MODE.REUSE);
            assertNotNull(token);
            assertEquals(token, MyCallback.stale);
        } catch (AcquireTokenException ex) {
            fail();
        }
    }

}
