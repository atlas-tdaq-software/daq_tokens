package tests;

import static org.junit.Assert.*;
import org.junit.Test;

import java.util.Map;

import daq.tokens.AcquireTokenException;
import daq.tokens.JWToken;
import daq.tokens.JWToken.MODE;
import daq.tokens.VerifyTokenException;
import daq.tokens.Credentials;

public class TestAcquire {

    public TestAcquire() {}

    @Test
    public void checkAquireOnly() throws AcquireTokenException
    {
        String token = JWToken.acquire(MODE.FRESH);
        assertNotNull(token);
    }

    @Test
    public void checkAcquireFresh() throws AcquireTokenException
    {
        String token1 = JWToken.acquire(MODE.FRESH);
        String token2 = JWToken.acquire(MODE.FRESH);
        assertNotNull(token1);
        assertNotNull(token2);
        assertNotEquals(token1.split(":")[0], token2.split(":")[0]);
    }

    @Test
    public void checkAcquireReuse() throws AcquireTokenException, VerifyTokenException
    {
        String token1 = JWToken.acquire(MODE.REUSE);
        String token2 = JWToken.acquire(MODE.REUSE);
        assertNotNull(token1);
        assertNotNull(token2);
        assertEquals(token1.split(":")[0], token2.split(":")[0]);
    }

    @Test
    public void checkAcquireVerify() throws AcquireTokenException, VerifyTokenException
    {
        String token = JWToken.acquire();
        Map<String, Object> result = JWToken.verify(token);
        System.out.println(result.get("sub"));
    }


    @Test
    public void checkVerifyTwice() throws AcquireTokenException, VerifyTokenException
    {
        String token = JWToken.acquire();
        Map<String, Object> result = JWToken.verify(token);
        System.out.println(result.get("sub"));
        try {
            result = JWToken.verify(token);
        } catch (VerifyTokenException ex) {
            System.out.println("Second verify() failed");
        }
    }

    @Test
    public void checkWithOperation() throws AcquireTokenException, VerifyTokenException
    {
        String token = JWToken.acquire("op://host/param");
        Map<String, Object> result = JWToken.verify(token, "op://host/param");
    }

    @Test
    public void checkCredential() throws AcquireTokenException, VerifyTokenException
    {
        String token = Credentials.get("op://host/param");
        String user  = Credentials.verify(token, "op://host/param");
    }
}
