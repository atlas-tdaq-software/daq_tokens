package tests;

import static org.junit.Assert.*;
import org.junit.Test;

import java.util.Map;
import java.util.Vector;

import daq.tokens.Hash;
import daq.tokens.HashException;

public class TestHash {

    public TestHash() {}

    @Test
    public void hash_int() throws HashException
    {
        String h = Hash.hash(1);
        assertNotNull(h);
        System.out.println("int " + 1 + " = " + h);
    }

    @Test
    public void hash_float() throws HashException
    {
        String h = Hash.hash(3.141);
        assertNotNull(h);
        System.out.println("float " + 3.141 + " = " + h);
    }

    @Test
    public void hash_string() throws HashException
    {
        String h = Hash.hash("Hello world");
        assertNotNull(h);
        System.out.println("Hello world = " + h);
    }

    @Test
    public void hash_multiple() throws HashException
    {
        String h = Hash.hash(1, 3.141, "Hello world");
        assertNotNull(h);
        System.out.println("tuple = " + h);
    }

    @Test
    public void hash_collection() throws HashException
    {
        Vector v = new Vector();
        v.add(1);
        v.add(2);
        v.add(3);
        String h = Hash.hash(v);
        assertNotNull(h);
        System.out.println("array = " + h);
    }
}
