
#include "daq_tokens/hash.h"
#include <iostream>
#include <vector>

namespace myns {
    struct custom {
        int x;
        float f;
        std::string s;
    };

    void hash_arg(EVP_MD_CTX *ctx, const myns::custom& arg)
    {
        using daq::tokens::detail::hash_arg;
        hash_arg(ctx, arg.x);
        hash_arg(ctx, arg.f);
        hash_arg(ctx, arg.s);
    }
}

int main()
{
    std::cout << "1 = " << daq::tokens::tuple_hash(1) << '\n';
    std::cout << "3.141 = " << daq::tokens::tuple_hash(3.141) << '\n';
    std::cout << "Hello world = " << daq::tokens::tuple_hash("Hello world") << '\n';
    std::cout << "tuple = " << daq::tokens::tuple_hash(1, 3.141, "Hello world") << '\n';
    std::cout << "array = " << daq::tokens::tuple_hash(std::vector<int>{1,2,3}) << '\n';
    std::string_view cv{"Hello"};
    std::string s = "world";
    std::vector<int> v{1, 2, 3};
    std::vector<std::string> sv{"hello", "world"};
    std::vector<const char *> psv{"Hello", "World"};
    myns::custom c{1, 6.2, "hi there"};
    auto h = daq::tokens::tuple_hash("test", s, cv, 5, 4.23, v, sv, psv, c);
    std::cout << h << std::endl;
}
