
#define CATCH_CONFIG_MAIN
#include "daq_tokens/credentials.h"

#if __has_include(<catch2/catch.hpp>)
#include <catch2/catch.hpp>
#else
#include <catch2/catch_all.hpp>
#endif

std::string
get(const std::string& operation = {})
{
    return daq::tokens::get_credentials(operation);
}

std::string
verify(const std::string& operation = {}, unsigned int max_age = 15)
{
    auto token = daq::tokens::get_credentials(operation);
    return daq::tokens::verify_credentials(token, operation, max_age);
}

std::string
too_old()
{
    auto token = daq::tokens::get_credentials("old:");
    sleep(3);
    return daq::tokens::verify_credentials(token, "old:", 1);
}


TEST_CASE( "Credentials", "[get_credentials]" ) {
    REQUIRE_FALSE( get().empty() );
    REQUIRE_FALSE( get("pmg:").empty() );
    REQUIRE_FALSE( verify().empty() );
    REQUIRE_FALSE( verify("pmg:").empty() );
    REQUIRE_FALSE( verify("pmg:", 5).empty() );
    REQUIRE_THROWS( too_old().empty() );
}

