
#include "daq_tokens/acquire.h"
#include <iostream>

bool equal(std::string_view a, std::string_view b)
{
    auto n = a.find(':');
    if(n != std::string_view::npos) a = a.substr(0, n);
    n = b.find(':');
    if(n != std::string_view::npos) b = b.substr(0, n);
    return a == b;
}

int main()
{
  try {
    using daq::tokens::acquire, daq::tokens::Mode;
    std::string token =  acquire(Mode::Reuse);

    if(token == "") {
       std::cerr << "Failed to acquire token" << std::endl;
       return EXIT_FAILURE;
    }

    if(!equal(token, acquire(Mode::Reuse))) {
      std::cerr << "Failed to acquire second reusable token" << std::endl;
      return EXIT_FAILURE;
    }

    std::string token2 = acquire(Mode::Fresh);
    if(token2 == "") {
      std::cerr << "Failed to acquire second fresh token" << std::endl;
      return EXIT_FAILURE;
    }

    std::cout << token2;

    std::string token3 = acquire("test1");
    std::string token4 = acquire(Mode::Fresh, "test2");

    return EXIT_SUCCESS;

  } catch(daq::tokens::Issue& ex) {
    std::cerr << ex << std::endl;
  }

  return EXIT_FAILURE;
}
