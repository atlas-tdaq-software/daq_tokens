
#include "daq_tokens/issues.h"
#include "daq_tokens/verify.h"
#include <iostream>
#include <cstdlib>

int main(int argc, char *argv[])
{
  using daq::tokens::verify;

  if(argc < 2) {
      std::cerr << argv[0] << ": expected token as first argument" << std::endl;
      return EXIT_FAILURE;
  }

  std::string operation;
  if(argc > 2) {
      operation = argv[2];
  }

  try {
      auto token = verify(argv[1], operation);

      std::cout << token.get_payload() << std::endl;

      try {
          auto same_token = verify(argv[1], operation);
      } catch(daq::tokens::CannotVerifyToken& ex) {
          // expected, replay cache should fail it
          std::cerr << ex << std::endl;
      }

      unsetenv("TDAQ_TOKEN_PUBLIC_KEY");
      setenv("TDAQ_TOKEN_PUBLIC_KEY_URL","http://not/a/valid/url", 1);

      try {
          auto token2 = verify(argv[1], operation);
          exit(EXIT_FAILURE);
      } catch(daq::tokens::CannotVerifyToken& ex) {
          // expected
          std::cerr << ex << std::endl;
      }

  } catch (daq::tokens::Issue& ex) {
    std::cerr << ex << std::endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;

}
