
#include "daq_tokens/acquire.h"

#include <chrono>
#include <iostream>

int main()
{
    using namespace daq::tokens;

    const int count = 1000;

    auto token = acquire();
    auto start = std::chrono::high_resolution_clock::now();
    for(int i = 0; i < count; i++) {
        token = acquire();
    }
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> time = end - start;
    std::cout << time.count()/count << " milliseconds/acquire (cached)" << std::endl;

    token = acquire(Mode::Fresh);
    start = std::chrono::high_resolution_clock::now();
    for(int i = 0; i < count; i++) {
       token = acquire(Mode::Fresh);
    }
    end = std::chrono::high_resolution_clock::now();
    time = end - start;
    std::cout << time.count()/count << " milliseconds/acquire (fresh)" << std::endl;
}
