// Generate a DPoP token
//
// make_dpop private_key.pem url [ access_token ]
//

#include "../src/DPoP.h"

#include <iostream>

void usage(std::string_view message = {})
{

    if(!message.empty()) {
        std::cerr << "make_dpop: " << message << std::endl;
    }
    std::cerr << "usage: make_dpop private_key.pem uri [ access_token ]\n"
              << "     private_key.pem - the PEM encoded Ed25519 key\n"
              << "     uri             - the URI encoding the operation\n"
              << "     access_token    - the access token to which the DPop token is bound [optional]" << std::endl;
}

int main(int argc, char *argv[])
{
    if(argc < 3) {
        usage("Not enough arguments");
        exit(1);
    }

    try {
        daq::tokens::details::DPoPKey key(argv[1]);
        std::cout << key.generate_dpop(argv[2], (argc > 3) ? argv[3] : "") << std::endl;
    } catch (std::exception& ex) {
        usage(std::string("Exception: ") + ex.what());
        exit(1);
    }

}
