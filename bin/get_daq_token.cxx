
#include "daq_tokens/acquire.h"

#include <iostream>

int main(int argc, char *argv[])
{
    daq::tokens::Mode mode = daq::tokens::Mode::Reuse;
    std::string op;

    argc--;
    argv++;

    while(argc > 0 && argv[0][0] == '-') {
        if(strcmp("-h", *argv) == 0 || strcmp("--help", *argv) == 0) {
            std::cerr << "usage: get_daq_token [ -f [ op ]]\n"
                      << "   -f   get a fresh token\n"
                      << "   op   DPoP operation\n\n";
            exit(0);
        }

        if(strcmp("-f", *argv) == 0) {
            mode = daq::tokens::Mode::Fresh;
        }

        argc--;
        argv++;
    }

    if(argc > 0) {
        op = *argv;
    }

    try {
        std::string token = daq::tokens::acquire(mode, op);
        std::cout << token;
    } catch(ers::Issue& ex) {
        std::cerr << ex << std::endl;
    } catch(std::exception& ex) {
        std::cerr << ex.what() << std::endl;
    }
}
