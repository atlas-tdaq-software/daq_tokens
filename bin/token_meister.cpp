#include "gssapi-utils/gssapi.h"
#include "../src/DPoP.h"
#include "jwt-cpp/traits/nlohmann-json/traits.h"

#include <jwt-cpp/jwt.h>
#include <jwt-cpp/traits/nlohmann-json/defaults.h>

#include <boost/asio.hpp>
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <openssl/crypto.h>
#include <openssl/evp.h>

#include <sstream>
#include <string>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <chrono>
#include <memory>
#include <filesystem>

#include <pwd.h>
#include <tuple>

typedef std::string private_key_t;

char hostname[HOST_NAME_MAX];

// interval in seconds when to recheck private key file
std::chrono::system_clock::duration check_key;

// flag to disable DPoP
bool enable_dpop = true;

void signal_handler(int /* sig */)
{
    exit(0);
}

// Create a SHA256 hash of the public key to be used as key identifer
std::string
get_fingerprint(const std::string& key, const std::string& digest)
{
    auto evp_priv = jwt::helper::load_private_key_from_string(key);

    unsigned char *data = nullptr;
    int size = i2d_PUBKEY(evp_priv.get(), &data);
    if(size < 0) { throw std::runtime_error("Cannot convert to DER"); }

    std::unique_ptr<unsigned char[], decltype([](unsigned char *p) { free(p); })> holder(data);

    // Create digest context, CentOS 8 has newer openssl version.
#if OPENSSL_VERSION_NUMBER < 0x10100000L
    std::unique_ptr<EVP_MD_CTX, decltype(&EVP_MD_CTX_destroy)> ctx(EVP_MD_CTX_create(), EVP_MD_CTX_destroy);
#else
    std::unique_ptr<EVP_MD_CTX, decltype(&EVP_MD_CTX_free)> ctx(EVP_MD_CTX_new(), EVP_MD_CTX_free);
#endif

    if(!ctx) { throw std::runtime_error("Cannot create MD context"); }

    if(!EVP_DigestInit(ctx.get(), EVP_get_digestbyname(digest.c_str()))) { throw std::runtime_error("Cannot initialize message digest"); }

    if(!EVP_DigestUpdate(ctx.get(), data, size)) { throw std::runtime_error("Cannot update message digest"); }

    std::unique_ptr<unsigned char[]> fingerprint(new uint8_t[EVP_MD_CTX_size(ctx.get())]);
    unsigned int length;

    if(!EVP_DigestFinal(ctx.get(), fingerprint.get(), &length)) {
        throw std::runtime_error("Cannot finalize message digest");
    }

    // Turn into hex
    std::stringstream fp;
    for(decltype(length) i = 0; i < length; i++) {
        fp << std::hex << std::setw(2) << std::setfill('0') <<  (unsigned int)fingerprint[i];
    }
    return fp.str();
}

std::tuple<std::string, std::string>
load_private_key(const std::string& path, const std::string& digest)
{
    static private_key_t key;
    static std::chrono::system_clock::time_point last_check;
    static std::filesystem::file_time_type last_write;
    static std::string fp;

    if(key.empty() || (check_key.count() != 0 &&
                       (std::chrono::system_clock::now() - check_key > last_check) &&
                       (std::filesystem::last_write_time(std::filesystem::path(path)) > last_write))) {
        key.clear();
        fp.clear();
        std::ifstream s{path.c_str()};
        if(s) {
            char buffer[32000];
            s.read(buffer, sizeof buffer - 1);
            buffer[s.gcount()] = '\0';
            key = buffer;
            last_check = std::chrono::system_clock::now();
            last_write = std::filesystem::last_write_time(std::filesystem::path(path));
            fp = get_fingerprint(key, digest);
            // std::cerr << "token_meister: (re-)loading private key" << std::endl;
        }
    }
    return std::make_tuple(key, fp);
}


// Create the token from the information given
std::string
make_token(const std::string& key_path, const std::string& digest, const std::string& user, const std::string& hostname, const std::string& cnf = {})
{
    auto uuid{boost::uuids::random_generator()()};

    auto now = std::chrono::system_clock::now();
    auto [key, fp] = load_private_key(key_path, digest);
    auto builder = jwt::create()
        .set_type("JWT")
        .set_issuer("https://auth.cern.ch/auth/realms/cern")
        .set_audience("atlas-tdaq-token")
        .set_issued_at(now)
        .set_expires_at(now + std::chrono::minutes{20})
        .set_not_before(now)
        .set_subject(user)
        .set_id(boost::uuids::to_string(uuid))
        .set_key_id(fp)
        .set_payload_claim("host", jwt::claim(hostname));
    if(enable_dpop && !cnf.empty()) {
        jwt::claim jkt{nlohmann::json::object({ { "jkt", cnf}})};
        builder.set_payload_claim("cnf", jkt);
    }

    return builder.sign(jwt::algorithm::rs256{"",key});
}

void
serve_local(const std::string& key_path, const std::string& digest, const std::string& socket_path)
{
    if(socket_path.size() > 107) {
        std::cerr << "token_meister: socket path too long (max 107 chars)\n";
        exit(1);
    }

    using namespace boost::asio::local;

    boost::asio::io_context ctx;
    stream_protocol::acceptor server_sock(ctx);

    try {
        if(getenv("LISTEN_FDS")) {
            // systemd socket activation
            server_sock = stream_protocol::acceptor(ctx, stream_protocol(), 3);
        } else {
            remove(socket_path.c_str());
            server_sock.open();
            server_sock.bind(stream_protocol::endpoint(socket_path));
            server_sock.listen(SOMAXCONN);
        }
    } catch(std::exception& ex) {
        std::cerr << ex.what() << std::endl;
        exit(1);
    }

    while(true) {
        try {
            stream_protocol::socket client(ctx);
            server_sock.accept(client);

            ucred credentials;
            socklen_t length = sizeof credentials;
            if(getsockopt(client.native_handle(), SOL_SOCKET, SO_PEERCRED, &credentials, &length) < 0) {
                perror("token_meister: getsockopt(SO_PEERCRED)");
                exit(1);
            }

            if(auto pw = getpwuid(credentials.uid)) {
                std::vector<char> buffer;
                boost::system::error_code rc;
                size_t client_length = boost::asio::read_until(client, boost::asio::dynamic_buffer( buffer), '\n', rc);
                std::string cnf;
                if(client_length > 0) {
                    // we got some data, interpret it as dpop+jwt
                    std::string dpop(buffer.data(), buffer.size() - 1);
                    cnf = daq::tokens::details::verify_dpop(dpop, socket_path);
                }
                auto token = make_token(key_path, digest, pw->pw_name, hostname, cnf);
                boost::asio::write(client, boost::asio::buffer(token.c_str(), token.size()));
            } else {
                std::cerr << "token_meister: no credentials for uid = " << credentials.uid << std::endl;
            }
        } catch(std::exception& ex) {
            std::cerr << ex.what() << std::endl;
        }
    }
}

void
serve_gssapi(const std::string& key_path, const std::string& digest, const std::string& port)
{
    using namespace gssapi_utils::net;
    using namespace std::literals::string_literals;

    io_context ctx;
    ip::tcp::acceptor server_socket(ctx);

    if(getenv("LISTEN_FDS")) {
        // systemd socket activation
        server_socket = ip::tcp::acceptor(ctx, ip::tcp::v6(), 3);
    } else {
        server_socket.open(ip::tcp::v6());

        server_socket.set_option(ip::v6_only(false));
        server_socket.set_option(socket_base::reuse_address(true));

        server_socket.bind(ip::tcp::endpoint(ip::tcp::v6(),
                                             (unsigned short)strtoul(port.c_str(), nullptr, 10)));
        server_socket.listen(SOMAXCONN);
    }

    while(true) {
        try {
            std::string user;
            auto sock = server_socket.accept();
            sock.set_option(ip::tcp::no_delay(true));
            auto ep   = sock.remote_endpoint();
            ip::tcp::resolver resolve(ctx);
            auto it = resolve.resolve(ep);
            gssapi_utils::context context{std::move(sock), user, "atdaqjwt"s};
            std::string token = make_token(key_path, digest, user.substr(0, user.find('@')), it.cbegin()->host_name());
            context.send(token.c_str(), token.size());
        } catch(std::exception& ex) {
            std::cerr << ex.what() << std::endl;
        }
    }
}

void time_it(const std::string& key_path, const std::string& digest, const std::string& user)
{
    const int count = 1000;

    auto start = std::chrono::high_resolution_clock::now();
    for(int i = 0; i < count; i++) {
        [[maybe_unused ]] auto token = make_token(key_path, digest, user, hostname);
    }
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> time = end - start;
    std::cout << time.count()/count << " milliseconds/token" << std::endl;
}

void usage()
{
    std::cerr
        << "usage: token_meister [--gssapi|--make|--time] [--hash=...] [--check-key=...] [--disable-dpop] /path/to/private/key /path/to/socket|port|user [hostname [dpop]]\n\n"
        << "   --local            run a local server provding tokens on /path/to/socket (DEFAULT)\n"
        << "   --gssapi           runs a GSSAPI server listening on TCP 'port'\n"
        << "   --make             generates a token for 'user' interactively\n"
        << "   --time             timing output to generate token for 'user'\n"
        << "   --hash=<HASH>      select hash function for finger print\n"
        << "   --check-key=<SECS> check if private key has changed and re-read it\n"
        << "   --disable-dpop     disable DPoP\n"
        << std::endl;
}

int
main(int argc, char *argv[])
{
    enum Mode { local, gssapi, make, timing } mode{local};

    if(argc < 2) {
        usage();
        exit(1);
    }

    argc--;
    argv++;

    OpenSSL_add_all_digests();
    std::string digest{"SHA256"};

    while(argc && **argv == '-') {
        if(strcmp(*argv, "--help") == 0) {
            usage();
            exit(0);
        } else if(strcmp(*argv, "--local") == 0) {
            mode = local;
            argv++;
            argc--;
        } else if(strcmp(*argv, "--gssapi") == 0) {
            mode = gssapi;
            argv++;
            argc--;
        } else if(strcmp(*argv, "--make") == 0) {
            mode = make;
            argv++;
            argc--;
        } else if(strcmp(*argv, "--time") == 0) {
            mode = timing;
            argv++;
            argc--;
        } else if(strcmp(*argv, "--disable-dpop") == 0) {
            enable_dpop = false;
            argv++;
            argc--;
        } else if(strncmp(*argv, "--check-key", 11) == 0) {
            if((*argv)[11] == '=') {
                check_key = std::chrono::seconds(std::stoul(*argv + 12));
            } else {
                argv++;
                argc--;
                if(argc == 0) {
                    std::cerr << "token_meister: --check-key requires argument" << std::endl;
                    exit(1);
                }
                check_key = std::chrono::seconds(std::stoul(*argv));
            }
            argv++;
            argc--;
        } else if(strncmp(*argv, "--hash", 6) == 0) {
            if((*argv)[6] == '=') {
                digest = *argv + 7;
            } else {
                argv++;
                argc--;
                if(argc == 0) {
                    std::cerr << "token_meister: --hash requires argument" << std::endl;
                    exit(1);
                }
                digest = *argv;
            }

            if(EVP_get_digestbyname(digest.c_str()) == nullptr) {
                std::cerr << "token_meister: invalid hash method: " << digest << std::endl;
                exit(1);
            }

            argv++;
            argc--;
        } else {
            std::cerr << "token_meister: invalid option: " << *argv << std::endl;
            usage();
            exit(1);
        }
    }

    if(argc < 1) {
        usage();
        exit(1);
    }

    std::string key_path = *argv;

    argv++;
    argc--;

    gethostname(hostname, sizeof hostname);

    signal(SIGTERM, signal_handler);

    std::string user;
    std::string cnf;

    switch(mode) {
    case local:
        serve_local(key_path, digest, argc ? *argv : "/run/tdaq_token");
        break;
    case gssapi:
        serve_gssapi(key_path, digest, argc ? *argv : "8991");
        break;
    case make:
        if(argc == 0) {
            std::cerr << "Expected user name" << std::endl;
            exit(1);
        }
        user = *argv;
        argc--;
        argv++;
        if(argc > 0) {
            strncpy(hostname, *argv, sizeof(hostname) - 1);
            argc--;
            argv++;
        }

        if(argc > 0) {
            cnf = daq::tokens::details::verify_dpop(*argv, argc > 1 ? argv[1] : "");
        }

        try {
            std::cout << make_token(key_path, digest, user, hostname, cnf);
        } catch(std::exception& ex) {
            std::cerr << "Could not create token: " << ex.what();
            exit(1);
        }
        break;
    case timing:
        if(argc == 0) {
            std::cerr << "Expected user name" << std::endl;
            exit(1);
        }
        time_it(key_path, digest, *argv);
        break;
    default:
        abort();
    }

    return 0;

}
