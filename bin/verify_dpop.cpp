// Verify just the DPoP token
//
// verify_dpop public_key.pem url [ access_token ]
//

#include "../src/DPoP.h"

#include <iostream>

void usage(std::string_view message = {})
{

    if(!message.empty()) {
        std::cerr << "verify_dpop: " << message << std::endl;
    }
    std::cerr << "usage: verify_dpop dpop-token [ uri [ access_token [max-age]]]\n"
              << "     dpop_token     - the DPoP proof to verify\n"
              << "     uri            - the URI encoding the operation\n"
              << "     access_token   - the access token to which the DPop proof is bound [optional]\n"
              << "     max-age        - maximum allowed age of the DPoP proof\n"
              << " Prints the thumbprint of the DPoP tokens key to stdout"
              << std::endl;
}

int main(int argc, char *argv[])
{
    if(argc < 2) {
        usage("Not enough arguments");
        exit(1);
    }

    try {
        std::cout << daq::tokens::details::verify_dpop(argv[1], argc > 2 ? argv[2] : "", (argc > 3) ? argv[3] : "", argc > 4 ? std::stoul(argv[4]) : 15) << std::endl;
    } catch (std::exception& ex) {
        std::cerr << std::string("Exception: ") + ex.what() << std::endl;
        exit(1);
    }

}
