# Server scripts

This script `token` is meant to be run as cgi script on
a web server. It should be protected by GSSAPI/Kerberos.

## Server setup

The server should run with minimum users and packages
installed.

```shell
sudo dnf install -y httpd mod_ssl mod_http2 mod_auth_gssapi cern-get-krb5-conf cern-get-certificate
```

Autoenroll your host and get a service keytab:

```shell
sudo cern-get-certificate --auto-enroll
sudo cern-get-keytab --service HTTP --isolate --keytab /etc/httpd.keytab
```

Set the certificate and private key options in `/etc/https/conf.d/ssl.conf` using
the correct hostname:

```
SSLCertificateFile /etc/pki/tls/certs/vm-atdaq-token.cern.ch.pem
SSLCertificateKeyFile /etc/pki/tls/private/vm-atdaq-token.cern.ch.key
```

Optionally: enable HTTP2 for your vhost:

```
Protocols h2 http/1.1
```

Open the firewall for HTTPS only:

```shell
sudo firewall-cmd --add-service https
sudo firewall-cmd --add-service https --permanent
sudo systemctl start httpd
```

## DAQ tokens setup

Install the `tdaq_tokens` RPM:

```shell
sudo rpm -ihv https://atlas-tdaq-repository.https://atlas-tdaq-repository.web.cern.ch/repository/tdaq-repo/tokens/tdaq_token-2.0.0-1.x86_64.rpm
```

Get the official TDAQ secret key from `~tdaqsw/keys/private.key` and copy it to `/etc/atdtoken/private.key`.
In your own installation generate a new key:

```shell
sudo openssl genrsa -out /etc/atdtoken/private.key
```

Protect the `/token` location with GSSAPI and redirect
it to the `/var/www/cgi-bin/token` script. See [token.conf](token.conf).

See [token](token) for the actual CGI script that generates the user token.
