
import unittest

from daq_tokens import acquire, verify
from jwt import InvalidAudienceError

class DaemonTest(unittest.TestCase):

    def setUp(self):
        self.token = acquire()

    def test_user_acquire(self):
        pass

    def test_user_verify_correct(self):
        decoded = verify(self.token)
        self.assertEqual(decoded['aud'], 'atlas-tdaq-token')
        self.assertEqual(decoded['iss'], 'https://auth.cern.ch/auth/realms/cern')

if __name__ == '__main__':
    unittest.main()
