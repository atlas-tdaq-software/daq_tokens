
import pydaq_tokens
from pydaq_tokens import acquire, Mode

import json
def verify(mode = Mode.Reuse, operation = '', max_age = 15):
    return json.loads(pydaq_tokens.verify(mode, operation, max_age))

# backward compat
FRESH = Mode.Fresh
REUSE = Mode.Reuse

Fresh = Mode.Fresh
Reuse = Mode.Reuse
__all__ = ['acquire', 'verify', 'FRESH', 'REUSE', 'Fresh', 'Reuse']
