
# Simple GSSAPI Utility Classes

The `gssapi_utils::context` class allows you to establish an
authenticated and encrypted connection between a
client and a server.

The following assumes the GSSAPI mechanism is Kerberos 5.

For both client and server the socket to communicate has
to be created and properly initialized/connected before
calling these routines.

## Client

In the client code create a `gssapi_utils::context` object by
specifying the service and host in the form `my-service@hostname.domain`.

You should have a valid kerberos ticket at this time, but this
is outside of the responsibility of the client program.

It will throw an exception if anything goes wrong.

```cpp
#include "gssapi_utils/gssapi.hpp"

int main()
{
   boost::asio::io_context ioctx;
   boost::asio::ip::tcp::socket sock(ioctx);
   // resolve hostname.domain and connect socket

   gssapi_utils::context ctx(sock, "my-service@hostname.domain");
   // ...
}
```

From now on you can send and receive messages which are
encrypted and authenticated.

```cpp
   const char msg[] = "Hello world";
   ctx.send(msg, sizeof(msg));

   size_t received_bytes = 0;
   auto received_data = ctx.receive(received_bytes);
   // do something with received_data.get() which is received_bytes long
```

The maximum message size is 4 GByte, but in practice it should be **much**
smaller. The receiver has to first receive the full encrypted data plus
authentication code, and then has to decrypt it in memory. One can clearly
see how this can be turned into a denial of service attack.

The current implementation splits large messages into frames of 64 kByte
by default. The maximum frame size can be modified with
`context::set_max_frame_size(size)`, but both sides have to agree on
a common value. If a frame with a larger size is received the connection
is dropped and a `protocol_error` exception is thrown on the
receiver's side.


### Options

The interface takes two additional options. One can pass a
boolean flag to indicate that the local credentials should be delegated
to the server. Note that the server can then use this to authenticate
on behalf of the client, so use with care, and only with servers
you completely trust.

One can also pass an explicit `gss_cred_t` credential parameter to the constructor.
This will replace the default credentials (typically your current kerberos ticket).
How this credential is acquired is outside of the scope of the library.

## Server

The server side code has to specify a `service` name if it does
not want to use the default service contained in its keytab file.
Use an empty service string `""` to indicate the default service (typically
`host`).

```cpp

void handle_connection(boost::asio::ip::tcp::socket sock)
{
   ...
   std::string client;
   gssapi_utils::context ctx(sock, client, "my-service");
   // 'client' format is USER@REALM
   while(sock.is_open()) {
      size_t length;
      std::string response;

      auto data = ctx.recv(length);
      // handle data, prepare response

      ctx.send(response.data(), response.size());
   }
}
```

### Creating a custom keytab entry

If the server is running as `root` and the host already has
a `/etc/krb5.keytab` file, simply specify `host` (or any other
service contained in that file) as service name in the client.

You can create your own `service` name for your application.
If you server runs as `root`, you can store the entry in
`/etc/krb5.keytab` and again do nothing special on the server
side. The client has to specify your service name when
establishing the connection.

On a generic Kerberos installation where you have the
necessary `kadmin` access, run this on the server host:

```shell
kadmin
> add_principal -randkey my-service/hostname
> ktadd -k /etc/kr5.keytab my-service/hostname
```

where `hostname` is your server's host name.

At CERN, run this on the server host:

```shell
sudo cern-get-keytab --service my-service
```

### Creating a custom keytab file

If you use your own service name and/or your server
does not run as `root`, it is better keep your
keytab separate from the host's.

```shell
kadmin
> add_principal -randkey my-service/hostname
> ktadd -k /tmp/my-service.keytab my-service/hostname
```

At CERN

```shell
cern-get-keytab --service my-service --isolate --keytab /tmp/my-service.keytab
```

Copy the `my-service.keytab` file to its final place, and make sure that
**only** your server application can read it.

Set the `KRB5_KTNAME` variable to the full path of the file before starting
the server.

```shell
export KRB5_KTNAME=FILE:/path/to/my-service.keytab
myserver
```
