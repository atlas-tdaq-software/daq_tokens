
#include "gssapi-utils/gssapi.h"

#include <gssapi/gssapi.h>
#include <string>
#include <array>
#include <sstream>
#include <iostream>

namespace gssapi_utils {

    const size_t more_flag = 0x80000000;

    // Internal helper routines for GSSAPI

    // Format GSSAPI error codes into an error message
    // A horror show all by itself...
    std::string
    format_error(OM_uint32 status_code, OM_uint32 minor)
    {
        std::stringstream ss;
        OM_uint32 message_context;
        OM_uint32 min_status, maj_status;
        gss_buffer_desc status_string;

        message_context = 0;

        do {
            maj_status = gss_display_status(&min_status,
                                            status_code,
                                            GSS_C_GSS_CODE,
                                            GSS_C_NO_OID,
                                            &message_context,
                                            &status_string);

            ss.write((char *)status_string.value, status_string.length);
            ss << '\n';
            gss_release_buffer(&min_status, &status_string);
        } while (message_context != 0 && maj_status == GSS_S_COMPLETE);
        ss << "\nMinor: " << minor;
        return ss.str();
    }

    // Receive data into an GSS buffer
    //
    // We expect a 4 byte length in little endian format first,
    // followed by the actual data.
    //
    // Caller must free memory with gss_release_buffer() or free()
    bool recv_buffer(net::ip::tcp::socket& s, gss_buffer_desc *buffer)
    {
        uint32_t length;

        error_code ec;
        // note: length is little-endian
        net::read(s, net::mutable_buffer(&length, sizeof length), ec);
        if(ec) {
            return false;
        }

        buffer->length = length;
        length &= ~more_flag;
        buffer->value  = (void *)malloc(length);

        if(buffer->value == nullptr) return -1;

        net::read(s, net::mutable_buffer(buffer->value, length), ec);
        if(ec) {
            free(buffer->value);
            return false;
        }

        return true;
    }

    // Send data in a GSS buffer.
    //
    // On the wire we send the length first as a 4 byte little endian value,
    // followed by actual data.
    bool send_buffer(net::ip::tcp::socket& s, const gss_buffer_desc *buffer)
    {
        uint32_t length = buffer->length & ~more_flag;

        // This satisfies boost::asio ConstBufferSequence traits
        std::array<net::const_buffer, 2> sendbuf
        {
            {
                { &length, sizeof length },
                { buffer->value, length }
            }
        };

        error_code ec;
        net::write(s, sendbuf, ec);

        return (ec) ? false : true;
    }

    // Create GSSAPI service name
    //
    // For server: just pass service name
    // For client: pass service name '@' host
    //
    // Caller must free name with gss_release_name
    gss_name_t
    make_service_name(const std::string& service_name)
    {
        gss_buffer_desc name_buf;
        gss_name_t      name;
        OM_uint32       maj_stat, min_stat;

        name_buf.value = (void *)service_name.c_str();
        name_buf.length = strlen((const char *)name_buf.value) + 1;
        maj_stat = gss_import_name(&min_stat, &name_buf,
                                   (gss_OID) GSS_C_NT_HOSTBASED_SERVICE, &name);
        if (maj_stat != GSS_S_COMPLETE) {
            throw gssapi_utils::cannot_establish_context(format_error(maj_stat, min_stat));
        }
        return name;
    }

    // Acquire server credentials
    //
    // Returns GSS_C_NO_CREDENTIAL on error, or if no name given
    //
    // Caller must free credentials with gss_release_cred
    //
    gss_cred_id_t
    acquire_server_credentials(const std::string& service_name)
    {
        if(service_name.empty()) {
            return GSS_C_NO_CREDENTIAL;
        }

        gss_name_t server_name = make_service_name(service_name);

        gss_cred_id_t   creds = GSS_C_NO_CREDENTIAL;
        OM_uint32       maj_stat, min_stat, ignore;
        maj_stat = gss_acquire_cred(&min_stat, server_name, 0,
                                        GSS_C_NULL_OID_SET, GSS_C_ACCEPT,
                                        &creds, NULL, NULL);
        gss_release_name(&ignore, &server_name);

        if(maj_stat != GSS_S_COMPLETE) {
            throw gssapi_utils::cannot_establish_context(format_error(maj_stat, min_stat));
        }

        return creds;
    }

    // s - socket to communicate with
    // service_name - the service name if non-standard, else empty (defaults to host/<HOSTNAME>@<DOMAIN>)
    // returns: client name (in user@DOMAIN format)
    //
    gss_ctx_id_t
    establish_server_context(net::ip::tcp::socket& s, const std::string& service_name, std::string& client_name, gss_cred_id_t& delegated)
    {
        auto server_creds = acquire_server_credentials(service_name);

        gss_ctx_id_t context = GSS_C_NO_CONTEXT;
        gss_buffer_desc send_tok, recv_tok;
        gss_name_t client;
        gss_OID doid;
        OM_uint32 maj_stat, min_stat, ignore, acc_sec_min_stat;
        OM_uint32 ret_flags;

        do {
            // server receives toke from client
            if (!recv_buffer(s, &recv_tok))
                break;

            maj_stat =
                gss_accept_sec_context(&acc_sec_min_stat,
                                       &context,
                                       server_creds,
                                       &recv_tok,
                                       GSS_C_NO_CHANNEL_BINDINGS,
                                       &client,
                                       &doid,
                                       &send_tok,
                                       &ret_flags,
                                       NULL,     /* ignore time_rec */
                                       &delegated);    /* ignore del_cred_handle */

            // always free the buffer we allocated
            gss_release_buffer(&ignore, &recv_tok);

            if (maj_stat != GSS_S_COMPLETE && maj_stat != GSS_S_CONTINUE_NEEDED) {
                // something went wrong
                if (context != GSS_C_NO_CONTEXT) {
                    gss_delete_sec_context(&ignore, &context,
                                           GSS_C_NO_BUFFER);
                    // context = GSS_C_NO_CONTEXT;
                }

                if(send_tok.length != 0) {
                    (void) gss_release_buffer(&ignore, &send_tok);
                }

                break;
            }

            // something to send back ?
            if (send_tok.length != 0) {
                bool ok = send_buffer(s, &send_tok);
                (void) gss_release_buffer(&ignore, &send_tok);
                if(!ok) {
                    break;
                }
            }

        } while (maj_stat == GSS_S_CONTINUE_NEEDED);

        // always release credentials, no matter the outcome
        if(server_creds != GSS_C_NO_CREDENTIAL) {
            gss_release_cred(&min_stat, &server_creds);
        }

        // if context was established
        if(context == nullptr) {
            throw gssapi_utils::cannot_establish_context(format_error(maj_stat, acc_sec_min_stat));
        }

        // translate name insto string
        gss_buffer_desc client_buf;
        maj_stat = gss_display_name(&min_stat, client, &client_buf, &doid);
        if (maj_stat != GSS_S_COMPLETE) {
            // do not continue if this failed
            (void) gss_delete_sec_context(&min_stat, &context,
                                          GSS_C_NO_BUFFER);
            context = GSS_C_NO_CONTEXT;
        } else {
            // success, copy out string and release buffer
            client_name.assign((char *)client_buf.value, client_buf.length);
            gss_release_buffer(&min_stat, &client_buf);
        }
        gss_release_name(&min_stat, &client);

        return context;
    }

    // Establish the GSSAPI client context
    // return nullptr on error
    gss_ctx_id_t
    establish_client_context(net::ip::tcp::socket& sock, const std::string& service_host, gss_cred_id_t credential, bool delegate_credential)
    {
        gss_name_t target_name = make_service_name(service_host);
        if(target_name == nullptr)
            return nullptr;

        OM_uint32 maj_stat, min_stat, ignore;

        gss_ctx_id_t    context{GSS_C_NO_CONTEXT};
        gss_buffer_t    input{GSS_C_NO_BUFFER};
        gss_buffer_desc input_token{0, nullptr};
        gss_buffer_desc output_token{0, nullptr};

        do {
            maj_stat =  gss_init_sec_context(&min_stat,
                                             credential,
                                             &context,
                                             target_name,
                                             GSS_C_NO_OID,
                                             GSS_C_MUTUAL_FLAG | GSS_C_REPLAY_FLAG | GSS_C_SEQUENCE_FLAG | GSS_C_CONF_FLAG |
                                             GSS_C_INTEG_FLAG | (delegate_credential ? GSS_C_DELEG_FLAG : 0),
                                             0,
                                             GSS_C_NO_CHANNEL_BINDINGS,
                                             input,
                                             nullptr,
                                             &output_token,
                                             nullptr,
                                             nullptr);

            // if had something as input, delete its buffer
            if(input != GSS_C_NO_BUFFER) {
                gss_release_buffer(&ignore, input);
            }

            // if there is something to send
            if(output_token.length != 0) {
                bool sent = send_buffer(sock, &output_token);
                gss_release_buffer(&ignore, &output_token);

                if(!sent) {
                    break;
                }
            }

            // if we continue receive next token as input
            if(maj_stat == GSS_S_CONTINUE_NEEDED) {
                if(!recv_buffer(sock, &input_token)) {
                    break;
                }
            }

            input = &input_token;

        } while(maj_stat == GSS_S_CONTINUE_NEEDED);

        gss_release_name(&ignore, &target_name);

        if(maj_stat != GSS_S_COMPLETE) {
            throw gssapi_utils::cannot_establish_context(format_error(maj_stat, min_stat));
            context = nullptr;
        }

        return context;
    }

}

namespace gssapi_utils {

    context::context(tcp::socket&& sock, std::string& client_name, const std::string& service_name)
        : m_socket(std::move(sock)),
          m_delegated(GSS_C_NO_CREDENTIAL),
          m_max_frame_size(default_max_frame_size),
          m_context(establish_server_context(m_socket, service_name, client_name, m_delegated))
    {}

    context::context(tcp::socket&& sock, const std::string& service_host_name, bool delegate_credential, gss_cred_id_t credential)
        : m_socket(std::move(sock)),
          m_delegated(GSS_C_NO_CREDENTIAL),
          m_max_frame_size(default_max_frame_size),
          m_context(establish_client_context(m_socket, service_host_name, credential, delegate_credential))
    {}

    context::~context()
    {
        OM_uint32 min;
        gss_delete_sec_context(&min, &m_context, GSS_C_NO_BUFFER);
        if(m_delegated != GSS_C_NO_CREDENTIAL) {
            gss_release_cred(&min, &m_delegated);
        }
    }

    bool context::send(const void *buffer, size_t length)
    {
        OM_uint32 maj, min;
        bool ret;

        while(length > m_max_frame_size) {
            gss_buffer_desc cleartext{m_max_frame_size, (void *)buffer};
            gss_buffer_desc encrypted{0, nullptr};
            maj = gss_wrap(&min, m_context, 1, GSS_C_QOP_DEFAULT, &cleartext, nullptr, &encrypted);
            if(maj != GSS_S_COMPLETE) {
                return false;
            }
            encrypted.length |= more_flag;
            ret = send_buffer(m_socket, &encrypted);
            gss_release_buffer(&min, &encrypted);

            buffer = (char *)buffer + m_max_frame_size;
            length -= m_max_frame_size;
        }

        gss_buffer_desc cleartext{length, (void *)buffer};
        gss_buffer_desc encrypted{0, nullptr};
        maj = gss_wrap(&min, m_context, 1, GSS_C_QOP_DEFAULT, &cleartext, nullptr, &encrypted);
        if(maj != GSS_S_COMPLETE) {
            return false;
        }
        ret = send_buffer(m_socket, &encrypted);
        gss_release_buffer(&min, &encrypted);

        return ret;
    }

    recv_ptr context::recv(size_t& buffer_length)
    {
        gss_buffer_desc encrypted;
        gss_buffer_desc cleartext;
        buffer_length = 0;
        bool more = false;

        char *tmp = nullptr;

        do {
            if(!recv_buffer(m_socket, &encrypted)) {
                if(tmp) free(tmp);
                return recv_ptr();
            }
            OM_uint32 maj, min;
            more = (encrypted.length & more_flag) != 0;
            encrypted.length &= ~more_flag;
            if(encrypted.length > m_max_frame_size) {
                m_socket.close();
                throw protocol_error("protocol error: frame too large");
            }
            maj = gss_unwrap(&min, m_context, &encrypted, &cleartext, nullptr, nullptr);
            tmp = (char *)realloc(tmp, buffer_length + cleartext.length);
            gss_release_buffer(&min, &encrypted);
            if(maj != GSS_S_COMPLETE) {
                if(tmp) free(tmp);
                return recv_ptr();
            }
            memcpy(&tmp[buffer_length], cleartext.value, cleartext.length);
            buffer_length += cleartext.length;
        } while(more);
        return recv_ptr(tmp);
    }

    tcp::socket context::release()
    {
        return std::move(m_socket);
    }

    gss_cred_id_t context::delegated_credential() const
    {
        return m_delegated;
    }

}
