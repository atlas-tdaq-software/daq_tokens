# DAQ Tokens

### DPoP (Demonstrating Proof of Possession) Support

DAQ tokens will contain an associated DPoP proof which will
be checked on the receiver side. This allows to associate
an access token with a certain operation, making it impossible
to replay a stolen/lost token against a different server
for a different functionality.

From the user's point of view this appears as an
additional optional string parameter to `acquire()` and
`verify()`. The argument should be a URI like string
characterizing the desired operation. It can be as simple
as a generic service indicator like `pmg:` in which case
it provides domain separation (e.g. such a token cannot
be used for changing trigger keys, or run control commands).
Or, it can contain the called method and arguments themselves,
in which case it can only be replayed with the exact same
parameters, e.g. `trg://ATLAS/setPrescales/3423/2532`

Furthermore the `verify()` function takes an optional
parameter `max_age` which specifies the maximum allowed age
of the DPoP proof. The default is 15 seconds (i.e. the token
is useless after 15 seconds). Since a DPoP proof is generated
by the client for each request its lifetime can be as short
or long as needed (defined by the server), up to the lifetime
of the access token itself

This concerns only the implementations of various CORBA
services that use the access manager for authorization
decisions and is transparent for everyone else.

### Helper functions for credentials

```cpp
#include "daq_tokens/credentials.h"
```

Use `daq::tokens::get_credentials()` to either acquire a token or just
get the current user name.

Use `daq::tokens::verify_credentials(creds)` to check the received credentials
and return the associated user name.

Both functions take an optional `operation` string parameter for use with
DPoP proofs. The verify function takes another optional integer parameter specifying
the maximum age of the DPoP proof in seconds (default is 15 seconds).

Similarly the `daq.tokens.Credentials` package in Java provides static
`get()` and `verify()` functions with multiple overloads to create
or verify credentials.

## tdaq-11-04-00

### Support `oidc-agent`

Add a new method (`agent`) to acquire a token from a running `oidc-agent`.
To make use of that the `oidc-agent-desktop` package has to be installed
on the system (e.g. lxplus). You have to start the agent manually
if your system is not configured to do it automatically.

```shell
eval $(oicd-agent-service use)
```

Note: This does not work on lxplus on the moment, use

```shell
eval $(oidc-agent)
```

Create an entry with the name `atlas-tdaq-token`:

  * `oidc-gen --flow=device --client-id atlas-tdaq-token -m --pub atlas-tdaq-token`
  * Select CERN as issuer.
  * Type return for scopes
  * Specify a password (not your CERN password...!)

Every time you want make use of the agent, make sure it is started
and then run

```shell
oidc-add atlas-tdaq-token
[enter your password]
```

Set the `TDAQ_TOKEN_ACQUIRE` environment variable to `agent`.
Use any interactive TDAQ commands as usual.

### Support local token cache

Add a new method to acquire token from a refresh token stored
in a cache file: 'cache'. This is intended for interactive use or
for long running jobs where a refresh token is acquired out of band
and provisioned for the specific use case.

Example:

```shell
export TDAQ_TOKEN_ACQUIRE="cache browser"
```

If set in an interactive shell, the first time a token is acquired
by any application the cache is empty. The browser will open a window
to let the user authenticate as usual. The refresh token returned from
this exchange is stored in the cache.

When another application requests a token, it will find the refresh token
in the cache and use it to acquire an access token, transparent for the user.
When the refresh token expires (at the end of the SSO session), the user
will be prompted again via the browser for authentication.

Instead of `browser` the `device` method can be given if the user session
is e.g. via `ssh` and not able to start a browser.

The refresh token can also be acquired out of band e.g. by using
the `sso-helper` functions. In particular one can request a token with
`offline_access` that will only expire if it hasn't been used for three months.

```shell
source ssh-helper.sh
token_from_browser atlas-tdaq-token | jq -r .refresh_token > $HOME/.cache/tdaq-sso-cache
chmod og-rwx $HOME/.cache/tdaq-sso-cache
```

The default cache location is `$HOME/.cache/tdaq-sso-cache` with a fallback to
`/tmp/tdaq-sso-cache-$USER` if the home directory does not exist. The file must
be **only** readable by the owner or it will be ignored. The location can also be
set explicitly by the defining the `TDAQ_TOKEN_CACHE` variable.

All this together can be used for a long running job that needs TDAQ authentication
like this:

```shell
source ssh-helper.sh
token_from_browser atlas-tdaq-token offline_access | jq -r .refresh_token > $HOME/.config/my-sso-cache
chmod og-rwx $HOME/.config/my-sso-cache
```

and then use it:

```shell
cm_setup
export TDAQ_TOKEN_CACHE=$HOME/.config/my-sso-cache
start-my-job
```

## tdaq-11-03-00

Add a new method to acquire a token if the user
has a kerberos ticket: 'http'. This connects
to a URL (default: `${TDAQ_TOKEN_HTTP_URL:-https://vm-atdaq-token.cern.ch/token}` )
which is protected by GSSAPI and will return a token
if the authentication succeeds. This is intended to
replace the `gssapi` mechanism going forward.

A PEM file can contain more than one public key. All
will be processed by `verify()`. This allows to rotate
in a new key while the old one is still valid without
changing `TDAQ_TOKEN_PUBLIC_KEY_URL` manually -
which is anyway impossible for already running processes.

## tdaq-10-00-00

The various Python based `token_meister` servers
have been replaced by a single C++ version.

The standard deployment for a local Unix domain socket:

```shell
token_meister /path/to/private.key [ /path/to/socket ]
token_meister --local /path/to/private.key [ /path/to/socket ]
```

The server using GSSAPI deployment requires
a Kerberos keytab for the `atdaqjwt` service.

```shell
cern-get-keytab --service atdaqjwt -o token.keytab
export KRB5_KTNAME=FILE:$(pwd)/token.keytab
token_meister --gssapi /path/to/private.key [ port ]
```

For interactively creating a token (for testing
and impersonating a user):

```shell
token_meister --make /path/to/private.key user
```
For internal timing test:

```shell
token_meister --time /path/to/private.key user
```

Additional options for all versions:

    --hash=<HASHNAME>

where `HASHNAME` is a valid OpenSSL name
for a hash function (e.g. 'SHA256'). To be
backward compatible with tdaq-09-04-00 use
`--hash=md5`.

The binary is statically linked against the stdc++
library and depends otherwise only on system libraries.
It can also be compiled independent from the TDAQ
software (see the [standalone](https://gitlab.cern.ch/atlas-tdaq-software/daq_tokens/-/tree/master/standalone) directory).
This means the binary can be just copied to a server
and run if all library dependencies are met.

The old Python based servers are still available.
They do not depend on any other TDAQ software which
can be useful in a system deployment.

They now live in their own `token_meister` package
and can be used like this:

```shell
python3 -m token_meister.local /path/to/key [ /path/to/socket ]
python3 -m token_meister.gssapi /path/to/key [ port ]
python3 -m token_meister.make /path/to/key user
```

### Command line interface to CERN SSO methods

The script allows to get a token for a (public) OAuth2 client via
the CERN SSO. The result is a JSON structure that should be further
processed with `jq` or similar tools.

```shell
python3 -m daq_tokens.cern_sso --client atlas-tdaq-token --krb5 -o save
token=$(jq .access_token < save)
```

Now you can use the `${token}` when accessing protected URLs:

```shell
curl -H "Authorization: Bearer ${token}" https://...
```

When the access token is expired, refresh it:

```shell
python3 -m daq_tokens.cern_sso --client atlas-tdaq-token --refresh $(jq .refresh_token < save) -o save
```

Other authorization grants:

```shell
python3 -m daq_tokens.cern_sso --client atlas-tdaq-token --browser -o save
python3 -m daq_tokens.cern_sso --client atlas-tdaq-token --password -o save
```

A new browser window will be opened for the `--browser` option, so you need a
graphical environment (or a text mode browser...).

The `--password` method is strongly discouraged. Don't type your central CERN password
into random scripts that ask for it.
