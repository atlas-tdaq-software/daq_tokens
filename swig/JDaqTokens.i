%module JDaqTokens

%{
#include "daq_tokens/common.h"
#include "daq_tokens/acquire.h"
#include "daq_tokens/verify.h"

#include <ers/Issue.h>

#include <sstream>


%}

%include <typemaps.i>
%include <stl.i>
%include <std_common.i>
%include <std_string.i>
%include <various.i>
%include <enums.swg>
 // SWIG 4.2 only
 // %include <std_string_view.i>
%{

static std::string verify(const std::string& token, const std::string& operation, unsigned int max_age)
{
    return daq::tokens::verify(token, operation, max_age).get_payload();
}

%}

%javaexception("daq.tokens.AcquireTokenException") daq::tokens::acquire {
  try {
    $action
  } catch(std::exception& ex) {
    jclass clazz = jenv->FindClass("daq/tokens/AcquireTokenException");

    const ers::Issue* issue = dynamic_cast<const ers::Issue*>(&ex);
    if(issue != nullptr) {
        std::ostringstream s;
        s << (*issue) << std::ends;

        const std::string& msg = s.str();
        jenv->ThrowNew(clazz, msg.c_str());
    } else {
        jenv->ThrowNew(clazz, ex.what());
    }

    return $null;
  }
}

%javaexception("daq.tokens.VerifyTokenException") verify {
  try {
    $action
  } catch(std::exception& ex) {
    jclass clazz = jenv->FindClass("daq/tokens/VerifyTokenException");

    const ers::Issue* issue = dynamic_cast<const ers::Issue*>(&ex);
    if(issue != nullptr) {
        std::ostringstream s;
        s << (*issue) << std::ends;

        const std::string& msg = s.str();
        jenv->ThrowNew(clazz, msg.c_str());
    } else {
        jenv->ThrowNew(clazz, ex.what());
    }

    return $null;
  }
}

%include "../daq_tokens/common.h"
%include "../daq_tokens/acquire.h"
std::string verify(const std::string& token, const std::string& operation, unsigned int max_age);

