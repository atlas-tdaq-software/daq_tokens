#ifndef DAQ_TOKENS_CREDENTIALS_H_
#define DAQ_TOKENS_CREDENTIALS_H_

#include <string>
#include "daq_tokens/acquire.h"

namespace daq {
    namespace tokens {

        /**
         * If tokens are enabled return a daq token else return the current process's user name.
         *
         * \param   mode      Can the token be cached, or must it be fresh (should be 'Reuse' if you don't know)
         * \param   operation A URL like string to describe the operation, implies a sender constraint token
         * \returns           An encoded token, or the plain user name
         * \throws            daq::tokens::CannotAcquiretoken
         *
         */
        std::string get_credentials(Mode mode = Mode::Reuse, std::string_view operation = {});
        inline
        std::string get_credentials(std::string_view operation)
        {
            return get_credentials(Mode::Reuse, operation);
        }

        /**
         * If tokens are enabled, verify the token and return the subject's name, else return 'creds' literally.
         *
         * \param creds      The credentials, either a token or a user name
         * \param operation  A URL like string to describe the operation, implies a sender constraint token
         * \param max_age    The maximum allowed age of the DPoP token in seconds
         * \returns          The verified user name from the token, or just 'creds'
         * \throws           daq::tokens::CannotVerifyToken
         */
        std::string verify_credentials(const std::string& creds, std::string_view operation = {}, unsigned int max_age = 15);
    }
}

#endif
