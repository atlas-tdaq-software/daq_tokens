#ifndef DAQ_TOKENS_VERIFY_H_
#define DAQ_TOKENS_VERIFY_H_

#include "daq_tokens/common.h"
#include "daq_tokens/issues.h"

#include <jwt-cpp/jwt.h>
#include <jwt-cpp/traits/nlohmann-json/defaults.h>

#include <string_view>

namespace daq {
  namespace tokens {

    using Token     = jwt::decoded_jwt<jwt::traits::nlohmann_json>;

    /// Verify an encoded ticket and provide an object if successful.
    ///
    /// \param encoded_token  A string containing the encoded token.
    /// \param operation A URL like string describing the operation, implies a sender constraint token
    /// \param max_age The maximum allowed age of the DPoP token in seconds
    ///
    /// \returns A decoded token.
    /// \throws daq::tokens::CannotVerify

      Token verify(std::string_view encoded_token, std::string_view operation = {}, unsigned int max_age = 15);
  }
}

#endif // DAQ_TOKENS_VERIFY_H_
