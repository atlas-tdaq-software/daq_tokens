#ifndef DAQ_TOKENS_ACQUIRE_H_
#define DAQ_TOKENS_ACQUIRE_H_

#include "daq_tokens/issues.h"
#include "daq_tokens/common.h"

// #include <functional>
#include <string>
#include <string_view>

namespace daq {
  namespace tokens {

    /// Flag to modify acquire() behaviour
    enum class Mode
      {
       // Get a new unique token
       Fresh,

       // Possibly get a cached token
       Reuse
      };

    /// Acquire a token.
    ///
    /// \param mode       If a new unique token is requested (Fresh) or a possibly cached one (Reuse, the default)
    /// \param operation  A string indicating the desired operation, implies a sender constraint token
    ///
    /// \returns          A string containing the encoded ticket.
    ///
    /// \throws           daq::tokens::CannotAcquireToken()
    ///
    std::string acquire(Mode mode = Mode::Reuse, const std::string& operation = {});

    inline
    std::string acquire(const std::string& operation)
    {
        return acquire(Mode::Reuse, operation);
    }

  }
}

#endif // DAQ_TOKENS_ACQUIRE_H_
