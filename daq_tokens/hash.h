#ifndef DAQ_TOKENS_HASH_
#define DAQ_TOKENS_HASH_

#include <openssl/evp.h>
#include <openssl/types.h>
#include <jwt-cpp/base.h>
#include <jwt-cpp/traits/nlohmann-json/defaults.h>
#include <string>
#include <string_view>
#include <utility>
#include <concepts>
#include <charconv>

namespace daq {
    namespace tokens {
        namespace detail {

            // strings are hashed with their size first and the
            // characters in the string afterwards
            inline
            void hash_arg(EVP_MD_CTX *ctx, std::string_view arg)
            {
                size_t size = arg.size();
                EVP_DigestUpdate(ctx, &size, sizeof(size));
                EVP_DigestUpdate(ctx, arg.data(), arg.size());
            }

            // just dispatch to string_view
            inline
            void hash_arg(EVP_MD_CTX *ctx, const std::string& arg)
            {
                hash_arg(ctx, std::string_view(arg));
            }

            // just dispatch to string_view
            inline
            void hash_arg(EVP_MD_CTX *ctx, const char *arg)
            {
                std::string_view s(arg);
                hash_arg(ctx, s);
            }

            // integral types
            template<typename T>
            requires std::integral<T> || std::floating_point<T>
            void hash_arg(EVP_MD_CTX *ctx, const T& arg)
            {
                char buffer[32];
                auto result = std::to_chars(buffer, buffer + sizeof(buffer), arg);
                *result.ptr = '\0';
                hash_arg(ctx, buffer);
            }

            // for containers the element count is hashed followed by
            // a hash of each element
            template<template<typename> typename CONT, typename T>
            void hash_arg(EVP_MD_CTX *ctx, const CONT<T>& arg)
            {
                size_t size = arg.size();
                EVP_DigestUpdate(ctx, &size, sizeof(size));
                for(auto& elem : arg) {
                    hash_arg(ctx, elem);
                }
            }

            template<typename ARG, typename ...ARGS>
            void hash_args(EVP_MD_CTX *ctx, ARG arg, ARGS... args)
            {
                using detail::hash_arg;
                hash_arg(ctx, arg);
                if constexpr (sizeof...(ARGS) > 0) {
                    hash_args(ctx, std::forward<ARGS>(args)...);
                }
            }

        }

        /**
         * A TupleHash like function using SHA256 for a list of arguments.
         *
         * Arguments are prefixed with their size before being hashed.
         * Supports:
         *
         *   - all primitive types (integral + floating point)
         *   - string, string_view, char * (interpreted as null terminated string)
         *   - CONTAINER<T> where T is one of the above
         *
         * Note that primitive types are converted to a string before being hashed,
         *
         * It will fail to compile with other types, like pointer to T (except T == char),
         * custom structs and classes etc. A user can declare a function *in his own namespace*
         *
         *    void hash_arg(EVP_MD_CTX *ctx, const T& arg)
         *
         * and implement a hash any way he/she likes.
         *
         * \param args...  arguments to be hashed
         * \returns        a base64url encoded string of the sha256 hash value[0..15]
         */
        template<typename ...ARGS>
        std::string tuple_hash(ARGS... args)
        {
            unsigned char result[32];
            std::unique_ptr<EVP_MD_CTX, decltype(&EVP_MD_CTX_free)> handle{EVP_MD_CTX_new(), &EVP_MD_CTX_free};
            auto ctx = handle.get();
            EVP_DigestInit(ctx, EVP_sha256());
            detail::hash_args(ctx, std::forward<ARGS>(args)...);
            unsigned int size = sizeof(result);
            EVP_DigestFinal(ctx, result, &size);
            std::string s{(char *)result, 16};
            return jwt::base::trim<jwt::alphabet::base64url>(jwt::base::encode<jwt::alphabet::base64url>(s));
        }
    }
}
#endif
