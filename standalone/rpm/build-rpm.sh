#!/bin/bash
[ $# -eq 1 ] || { echo "usage: $0 <version>"; exit 1; }
out=$(readlink -f $(dirname $0))
version="$1"
dnf install -y rpmdevtools rpm-build
rpmdev-setuptree
pushd ~/rpmbuild/SOURCES
curl -O https://gitlab.cern.ch/atlas-tdaq-software/daq_tokens/-/archive/${version}/daq_tokens-${version}.tar.gz
popd
pushd ~/rpmbuild/SPECS
tar --strip-components=3 -zxf ~/rpmbuild/SOURCES/daq_tokens-${version}.tar.gz daq_tokens-${version}/standalone/rpm/tdaq_token.spec
sed -i "s;^Version:.*;Version: ${version};" tdaq_token.spec
dnf install -y 'dnf-command(builddep)'
dnf builddep -y tdaq_token.spec
rpmbuild -ba tdaq_token.spec
popd
cp ~/rpmbuild/RPMS/$(arch)/*.rpm ~/rpmbuild/SRPMS/*.rpm ${out}
