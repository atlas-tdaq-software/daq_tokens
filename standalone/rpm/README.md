# RPM Generation from scratch

Change version as appropriate.

```shell
version=v1.0.6
cd daq_tokens/standalone/rpm
podman run -it --rm -v $(pwd):/mnt almalinux:9 /mnt/build-rpm.sh $version
```

The binary and source RPMs will be in the current directory.
