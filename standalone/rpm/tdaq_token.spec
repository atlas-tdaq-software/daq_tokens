Name:           tdaq_token
Summary:        The ATLAS TDAQ token server
Version:        v1.0.6
Release:        1%{?dist}
License:        Apache 2.0
Vendor:         ATLAS TDAQ
Source:         https://gitlab.cern.ch/atlas-tdaq-software/daq_tokens/-/archive/%{version}/daq_tokens-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  openssl-devel krb5-devel gcc-c++ libstdc++-devel cmake make

# Requires:       openssl-libs krb5-libs pcre

Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd

%description
This package provides the ATLAS TDAQ token server
as a stand-alone binary, together with the
associated systemd files.

%prep
%setup -q -n daq_tokens-%{version}

%build
%cmake -D WITH_STATIC_LIBS=OFF -S %{_builddir}/daq_tokens-%{version}/standalone
%cmake_build

%install
%cmake_install

%clean
rm -rf $RPM_BUILD_ROOT

%post

%systemd_post tdaq_token.socket
%systemd_post tdaq_token.service
%systemd_post tdaq_token_gssapi.socket
%systemd_post tdaq_token_gssapi.service

%postun
%systemd_postun_with_restart tdaq_token.socket
%systemd_postun_with_restart tdaq_token.service
%systemd_postun_with_restart tdaq_token_gssapi.socket
%systemd_postun_with_restart tdaq_token_gssapi.service

%preun
%systemd_preun tdaq_token.socket
%systemd_preun tdaq_token.service
%systemd_preun tdaq_token_gssapi.socket
%systemd_preun tdaq_token_gssapi.service

%files
%defattr(-,root,root,-)
/usr/bin/token_meister
/usr/lib/systemd/system/tdaq_token.service
/usr/lib/systemd/system/tdaq_token.socket
/usr/lib/systemd/system/tdaq_token_gssapi.service
/usr/lib/systemd/system/tdaq_token_gssapi.socket

%changeLog
