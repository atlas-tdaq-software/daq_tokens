# Build a stand alone executable of `token_meister` server

On a recent system like RHEL/Alma/CentOS 9

```shell
git clone https://gitlab.cern.ch/atlas-tdaq-software/daq_tokens.git
cmake -S daq_tokens/standalone -B build
cmake --build build -j $(nproc)
```

We require at least g++ 11, CMake 3.14 and support of the -std=c++20 flag.

The resulting executable is statically linked against
libstdc++ and libgcc, so at run time it does not
depend on the exact compiler version.

## Compilers/CMake from devtoolset on CentOS 7 (deprecated)

You can also install additional toolsets:

```shell
sudo yum install -y \
    devtoolset-11-gcc-c++
    devtoolset-11-libstdc++-devel
sudo yum install -y epel-release
sudo yum install cmake3
scl enable devtoolset-11 bash
mkdir build; cd build
cmake3 /path/to/daq_tokens/standalone
make
```

### System Installation

To install the compiled binary and associated systemd files on the host

```shell
cmake -D CMAKE_INSTALL_PREFIX=/usr -B build -S daq_tokens/standalone
cmake --build build -j $(nproc)
sudo cmake --install build
```

But you should prefer to create an RPM and install that.

### RPM

To generate an RPM from the cmake build:

```shell
cd build
cpack -G RPM
```

### RPM Installation

To install the generated RPM

```shell
rpm -ihv tdaq_token-1.0.5.rpm
```

### System Configuration

To generate the keys (or copy them if they are cluster wide)

```shell
sudo openssl genrsa -out /etc/atdtoken/private.key
sudo openssl rsa -in /etc/atdtoken/private.key -pubout > /etc/atdtoken/public.key
```
The default location for the private key is `/etc/atdtoken/private.key` (should be only
readable by root). If another location is desired, a drop in file
`/etc/systemd/system/tdaq_token.d/private-key.conf` should be created:

```shell
LoadCredential=private.key:/path/to/private.key
```

#### GSSPI configuration (optional)

To generate the keytab (if you want to use the GSSAPI server):

```shell
sudo cern-get-keytab --service atdaqjwt --isolate -k /etc/atdtoken/token.keytab
export KRB5_NTFILE=FILE:/etc/atdtoken/token.keytab
```

### Services

```shell
sudo systemctl enable --now tdaq_token.socket
```

If you are running the gssapi server:

```shell
sudo systemctl enable --now tdaq_token_gssapi.socket
sudo firewall-cmd --add-port 8891/tcp
sudo firewall-cmd --add-port 8891/tcp --permanent
```

If you are wondering if you need the latter, the answer is no.
